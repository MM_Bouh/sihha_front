import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import clsx from "clsx";

const Search = ({ onSearch }) => {
  const [search, setSearch] = useState("");
  const [searchStatus3, setSearchStatus3] = useState(false);
  const toggleSearch3 = () => setSearchStatus3(!searchStatus3);

  const onInputChange = (value) => {
    setSearch(value);
    onSearch(value);
  };
  return (
    <div
      className={clsx("search-wrapper search-wrapper--expandable", {
        "is-active": searchStatus3,
      })}
    >
      <span className="icon-wrapper text-black" htmlFor="header-search-input">
        <FontAwesomeIcon icon={["fas", "search"]} color={"#3c44b1"} />
      </span>
      <input
        onFocus={toggleSearch3}
        onBlur={toggleSearch3}
        className="form-control"
        id="header-search-input"
        name="header-search-input"
        placeholder="Doctor's name"
        type="search"
        //data={doctors}
        value={search}
        onChange={(e) => onInputChange(e.target.value)}
      />
    </div>
  );
};

export default Search;
