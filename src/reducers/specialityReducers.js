import axios from "axios";
import {
  SPECIALITY_LIST_REQUEST,
  SPECIALITY_LIST_SUCCESS,
  SPECIALITY_LIST_FAIL,
} from "../actions/actionTypes";

export const specialityListReducer = (state = { specialities: [] }, action) => {
  switch (action.type) {
    case SPECIALITY_LIST_REQUEST:
      return { loading: true, specialities: [] };

    case SPECIALITY_LIST_SUCCESS:
      return {
        loading: false,
        specialities: action.payload.specialities,
      };

    case SPECIALITY_LIST_FAIL:
      return { loading: false, error: action.payload };

    default:
      return state;
  }
};
