import axios from "axios";
import { HOST_URL } from "../settings";
import * as actionTypes from "./actionTypes";

export const addMessage = (message) => {
  return {
    type: actionTypes.ADD_MESSAGE,
    message: message,
  };
};

export const setMessages = (messages) => {
  return {
    type: actionTypes.SET_MESSAGES,
    messages: messages,
  };
};

export const setLoading = (loading) => {
  return {
    type: actionTypes.LOADING_MESSAGES,
    loading: true,
  };
};

const getUserChatsSuccess = (chats) => {
  return {
    type: actionTypes.GET_CHATS_SUCCESS,
    chats: chats,
  };
};

export const getUserChats = (token) => {
  return (dispatch) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `JWT ${token}`,
      },
    };
    axios
      .get(`${HOST_URL}/chat/`, config)
      .then((res) => dispatch(getUserChatsSuccess(res.data)));
  };
};
