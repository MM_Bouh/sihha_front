import axios from "axios";
import Modali, { useModali } from "modali";
import React, { useEffect, useMemo, useState } from "react";
import { FaCommentMedical } from "react-icons/fa";
import { connect } from "react-redux";
import { Heart } from "react-spinners-css";
import { useHistory } from "react-router-dom";
//Toaster Code
import { toast } from "react-toastify";
import { Button, Card, Col, Row } from "reactstrap";
import * as doctorActions from "../../../actions/doctorActions";
import SuccessSound from "../../assets/success-sound-effect.mp3";
import Message from "../Message";
//import Pagination from "../../../containers/Pagination";
import Pagination from "../Pagination";
import Search from "../Search";
import Sort from "../Sort";

const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};

function AvailableDoctors(props) {
  const doctorList = props.doctors;
  const { loading, error, doctors } = doctorList;

  const history = useHistory();

  // For thhe new pagination
  const [totalItems, setTotalItems] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [sorting, setSorting] = useState({ field: "", order: "" });

  const fields = [
    { name: "Speciality", field: "specialityName", sortable: true },
    { name: "Experience", field: "experience", sortable: true },
    //{ name: "Username", field: "username", sortable: false },
  ];

  const ITEMS_PER_PAGE = 3;

  const doctorsData = useMemo(() => {
    let computedDoctors = doctors;
    if (search) {
      computedDoctors = computedDoctors.filter((doctor) =>
        doctor.username.toLowerCase().includes(search.toLowerCase())
      );
    }
    setTotalItems(computedDoctors.length);

    //Sorting doctors
    if (sorting.field) {
      const reversed = sorting.order === "asc" ? 1 : -1;
      computedDoctors = computedDoctors.sort(
        (a, b) =>
          reversed *
          a[sorting.field]
            .toString()
            .localeCompare(b[sorting.field], undefined, {
              numeric: true,
            })
      );
    }

    //Current Page slice
    return computedDoctors.slice(
      (currentPage - 1) * ITEMS_PER_PAGE,
      (currentPage - 1) * ITEMS_PER_PAGE + ITEMS_PER_PAGE
    );
  }, [doctors, currentPage, search, sorting]);

  //

  const cardStyle = {
    transition: "all 0.3s ease-in",
  };
  const writeStyle = {
    color: "#5A20CB",
  };
  const titleStyle = {
    color: "#1B98F5",
  };

  const SuspenseLoading = () => {
    return (
      <>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <Heart color={"#3c44b1"} />
          </div>
        </div>
      </>
    );
  };
  useEffect(() => {
    props.listDoctors();
  }, []);
  const handleDoctorName = (name) => {
    console.log("The doctor's name : ", name);

    setDoctorName(name);
  };
  const handleDoctorId = (id) => {
    console.log("The doctor's id : ", id);
    setDoctorId(id);
  };
  const [doctorName, setDoctorName] = useState("");
  const [doctorId, setDoctorId] = useState(null);
  const [completeModal, toggleCompleteModal] = useModali({
    centered: true,
    animated: true,
    // title: "Are you sure?",
    large: true,
    title:
      "Do you really want to have a conversation with Dr " + doctorName + " ?",
    message:
      "Please, bear in mind that your conversation will be pending until the administrator approves it.",
    // message: "Deleting this user will be permanent.",
    buttons: [
      <Modali.Button
        label="Cancel"
        isStyleCancel
        onClick={() => toggleCompleteModal()}
      />,
      <Modali.Button
        label="Create"
        //isStyleDestructive
        isStyleDefault
        onClick={() => createConversation()}
      />,
    ],
  });
  const createConversation = () => {
    //config
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    //body
    const body = {
      doctor: doctorId,
      patient: props.user,
      admin_agent: "26721541",
    };
    axios
      // .post('https://medicare-back.herokuapp.com/chat-api/create_conversation/',body,config)
      .post(
        "http://localhost:8000/chat-api/create_conversation_by_patient/",
        body,
        config
      )
      .then((res) => {
        if (res.data["error"]) {
          const sound = new Audio(SuccessSound);
          sound.volume = 0.008;
          sound.play();
          successNotify(
            "fail",
            "Sorry, you already have a conversation going on with this doctor!"
          );
        } else {
          const sound = new Audio(SuccessSound);
          sound.volume = 0.007;
          sound.play();
          successNotify("success", "Consultation Created");
          setTimeout(() => {}, [3000]);
          // window.location = "/Doctors";
          history.push("/Doctors");
        }
      });
  };

  return (
    <>
      <div>
        <Row className="mb-3">
          <Col xs={4} md={4} lg={4}>
            <Search
              onSearch={(value) => {
                setSearch(value);
                setCurrentPage(1);
              }}
            />
          </Col>
          <Col xs={4} md={4} lg={4}>
            <div
              className="font-size-xl font-weight-bold pt-1 pb-3 text-center"
              style={titleStyle}
            >
              AVAILABLE DOCTORS
            </div>
          </Col>
          <Col xs={4} md={4} lg={4}>
            <Sort
              headers={fields}
              onSorting={(field, order) => setSorting({ field, order })}
            />
          </Col>
        </Row>

        {loading ? (
          <SuspenseLoading />
        ) : error ? (
          <Message variant="danger">{error}</Message>
        ) : (
          <Row>
            {doctorsData.map((doctor) => (
              <Col md="4" key={doctor.id}>
                <Card
                  className="card-box mb-5 hover-scale-rounded"
                  style={cardStyle}
                >
                  <div className="text-center py-3">
                    <div className="d-90 rounded-circle border-0 my-2 card-icon-wrapper bg-plum-plate btn-icon mx-auto text-center">
                      <div className="avatar-icon-wrapper d-80">
                        <div className="avatar-icon d-80 rounded-circle">
                          <img alt="..." src={doctor.profile_img_url} />
                        </div>
                      </div>
                    </div>
                    <div
                      className="font-size-xl font-weight-bold pt-2"
                      style={writeStyle}
                    >
                      Dr. {doctor.username}
                    </div>
                    <br />
                    <span className="opacity-5 pb-3">Speciality: </span>
                    <span
                      className="font-size-md font-weight-bold pt-2 text-black"
                      style={writeStyle}
                    >
                      {doctor.specialityName}
                    </span>

                    <div className="divider mx-auto w-50 my-3" />
                    <span className="opacity-5 pb-3">Experience: </span>
                    <span
                      className="font-size-md font-weight-bold pt-2 text-black"
                      style={writeStyle}
                    >
                      {doctor.experience}{" "}
                      {doctor.experience > 1 ? "years" : "year"}
                    </span>
                    <div className="divider mx-auto w-50 my-3" />
                    <div className="text-center">
                      <Button
                        className="p-0 d-50 btn-icon btn-pill mx-1 btn-transition-none"
                        color="first"
                        outline
                        onClick={() => {
                          handleDoctorId(doctor.id);
                          handleDoctorName(doctor.username);
                          toggleCompleteModal();
                        }}
                      >
                        <span className="btn-wrapper--icon">
                          <FaCommentMedical />
                        </span>
                      </Button>
                      <Modali.Modal {...completeModal} />
                    </div>
                  </div>
                </Card>
              </Col>
            ))}
          </Row>
        )}

        <Row>
          <Col
            md="12"
            className="d-flex align-items-center justify-content-center ml-6"
          >
            {/* <Pagination
              totalRecords={totalDoctors}
              pageLimit={3}
              pageNeighbours={1}
              onPageChanged={onPageChanged}
            /> */}
            <Pagination
              total={totalItems}
              itemsPerPage={ITEMS_PER_PAGE}
              currentPage={currentPage}
              onPageChange={(page) => setCurrentPage(page)}
            />
          </Col>
        </Row>
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    doctors: state.doctors,
    user: state.auth.user.id,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listDoctors: () => dispatch(doctorActions.listDoctors()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AvailableDoctors);
