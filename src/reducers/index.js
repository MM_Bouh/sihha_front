import ThemeOptions from "./ThemeOptions";
import auth from "./auth";
import { combineReducers } from "redux";
import messageReducer from "./message";
import { doctorListReducer } from "./doctorReducers";
import { specialityListReducer } from "./specialityReducers";

export default combineReducers({
  ThemeOptions,
  auth,
  message: messageReducer,
  doctors: doctorListReducer,
  specialities: specialityListReducer,
});
