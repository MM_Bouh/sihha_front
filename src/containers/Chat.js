import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import clsx from "clsx";
import { Circle, Heart } from "react-spinners-css";
import "./chat.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Row,
  Col,
  CardBody,
  Card,
  CardHeader,
  Input,
  Badge,
  UncontrolledTooltip,
  Nav,
  NavItem,
  Button,
  Table,
} from "reactstrap";
import { RiCloseCircleFill } from "react-icons/ri";
import { AiOutlineBars } from "react-icons/ai";
import { FaPaperPlane } from "react-icons/fa";

import { NavLink as NavLinkStrap } from "reactstrap";
import { Link } from "react-router-dom";

// import {PerfectScrollbar} from "react-perfect-scrollbar";
import PerfectScrollbar from "@opuscapita/react-perfect-scrollbar";
//import "react-perfect-scrollbar/dist/css/styles.css";

import * as messageActions from "../actions/message";
import WebSocketInstance from "../websocket";
import Hoc from "../hoc/hoc";

import { connect } from "react-redux";

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function Chat(props) {
  const waitForSocketConnection = (callback) => {
    //const component = this;
    setTimeout(function () {
      if (WebSocketInstance.state() === 1) {
        console.log("Connection is made");
        callback();
        return;
      } else {
        console.log("wait for connection...");
        waitForSocketConnection(callback);
      }
    }, 100);
  };
  const messageR = useRef(null);

  const initialiseChat = () => {
    waitForSocketConnection(() => {
      console.log("These are the messages: ", props.messages);

      WebSocketInstance.fetchMessages(props.tel, props.match.params.chatID);
    });
    console.log("These are the messages: ", props.messages);
    WebSocketInstance.connect(props.match.params.chatID);
  };

  const currentProps = props.match.params?.chatID;
  const prevProps = usePrevious(currentProps);

  //const [loading, setLoading] = useState(true);
  //const [isThereMessages, setIsThereMessages] = useState(false);
  const thereAreMessages = props.messages.length > 0;

  const SuspenseLoading = () => {
    return (
      <>
        <div className="align-items-center flex-wrap vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <Heart
              color={"#3c44b1"}
              //loading={true}
            />
          </div>
        </div>
      </>
    );
  };

  useEffect(() => {
    initialiseChat();

    //console.log(props.location.state)
    WebSocketInstance.addCallbacks(props.setMessages, props.addMessage);
    //setIsThereMessages(true);
    //setLoading(false);

    if (prevProps !== currentProps) {
      WebSocketInstance.disconnect();
      waitForSocketConnection(() => {
        WebSocketInstance.fetchMessages(props.tel, currentProps);
      });
      WebSocketInstance.connect(currentProps);
    }
    if (messageR) {
      messageR.current.addEventListener("DOMNodeInserted", (event) => {
        const { currentTarget: target } = event;
        target.scroll({ top: target.scrollHeight, behavior: "smooth" });
      });
    }
  }, [currentProps]);

  //States
  //const [messages, setMessages] = useState(undefined);
  const [message, setMessage] = useState("");

  //LAYOUT STATES
  const [isSidebarMenuOpen2, setIsSidebarMenuOpen2] = useState(false);

  const toggleSidebarMenu2 = () => setIsSidebarMenuOpen2(!isSidebarMenuOpen2);

  const [activeTab, setActiveTab] = useState("1");

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const [inputBg, setInputBg] = useState(false);
  const toggleInputBg = () => setInputBg(!inputBg);
  ////////////////////////////

  const messageChangeHandler = (event) => {
    //this.setState({ message: event.target.value });
    setMessage(event.target.value);
  };

  const sendMessageHandler = (e) => {
    e.preventDefault();
    const messageObject = {
      from: props.tel,
      content: message,
      chatId: props.match.params.chatID,
    };
    if (message === "") {
      console.log("hello null");
      return null;
    }
    WebSocketInstance.newChatMessage(messageObject);
    //let items = [...props.messages, messageObject];
    // console.log("these are the items:", items)
    // props.setMessages(items)
    setMessage("");
  };

  const renderTimestamp = (timestamp) => {
    let prefix = "";
    const timeDiff = Math.round(
      (new Date().getTime() - new Date(timestamp).getTime()) / 60000
    );
    if (timeDiff < 1) {
      // less than one minute ago
      prefix = "just now...";
    } else if (timeDiff < 60 && timeDiff > 1) {
      // less than sixty minutes ago
      prefix = `${timeDiff} minutes ago`;
    } else if (timeDiff < 24 * 60 && timeDiff > 60) {
      // less than 24 hours ago
      prefix = `${Math.round(timeDiff / 60)} hours ago`;
    } else if (timeDiff < 31 * 24 * 60 && timeDiff > 24 * 60) {
      // less than 7 days ago
      prefix = `${Math.round(timeDiff / (60 * 24))} days ago`;
    } else {
      prefix = `${new Date(timestamp)}`;
    }
    return prefix;
  };
  const colorStyle = {
    backgroundColor: "#5A20CB",
    color: "white",
  };
  const colorStyleP = {
    backgroundColor: "#120E43",
    color: "white",
  };

  const divStyle = {
    position: "relative",
    marginTop: "-7%",
    height: "60px",
    //marginBottom: "-15px"
  };
  const messStyle = {
    color: "#6A1B4D",
  };

  const buttonStyle = {
    width: "80%",
  };
  const buttonS = {
    color: "#E21717",
  };

  return (
    <React.Fragment>
      <div
        className="app-inner-content-layout app-inner-content-layout-fixed"
        style={divStyle}
      >
        <div className="btn-md-pane d-lg-none px-2 order-0">
          {isSidebarMenuOpen2 ? (
            <div>
              <Button
                onClick={toggleSidebarMenu2}
                size="sm"
                //style={buttonS}
                color="danger"
                className="p-0 btn-icon d-40 mb-2"
                //id="closeSidebar"
              >
                <RiCloseCircleFill />
              </Button>
              {/* <UncontrolledTooltip target="closeSidebar">
                Close the sidebar
              </UncontrolledTooltip> */}
            </div>
          ) : (
            <div>
              <Button
                onClick={toggleSidebarMenu2}
                size="sm"
                color="primary"
                className="p-0 btn-icon d-40 mb-2"
                //id="openSidebar"
              >
                {/* <AiOutlineBars /> */}
                <FontAwesomeIcon icon={["fas", "bars"]} />
              </Button>
              {/* <UncontrolledTooltip target="openSidebar">
                Open the sidebar
              </UncontrolledTooltip> */}
            </div>
          )}
        </div>

        <div className="app-inner-content-layout--main order-3 order-lg-2 card-box bg-secondary">
          <CardHeader className="rounded-0 bg-white p-4 border-bottom">
            <div className="card-header--title">
              <small>Messenger</small>
              {props.user?.role === "PATIENT" && (
                <b>Talking to Dr. {props.location.state.doctor_username}</b>
              )}
              {props.user?.role === "DOCTOR" && (
                <b>
                  Talking to Patient. {props.location.state.patient_username}
                </b>
              )}
            </div>
          </CardHeader>
          <div className="messages" ref={messageR}>
            {props.loading ? (
              <SuspenseLoading />
            ) : (
              <div className="chat-wrapper ">
                {thereAreMessages ? (
                  <div>
                    {props.messages.map((message) => {
                      if (message.author === props.user?.id) {
                        return (
                          <div
                            key={message.id}
                            className="chat-item chat-item-reverse p-2 mb-2"
                          >
                            <div className="align-box-row flex-row-reverse">
                              <div className="avatar-icon-wrapper avatar-icon-lg align-self-start">
                                <div className="avatar-icon rounded-circle shadow-none">
                                  <img
                                    alt="..."
                                    src={props.user.profile_img_url}
                                  />
                                </div>
                              </div>
                              <div>
                                <div
                                  className="chat-box text-justify"
                                  style={colorStyleP}
                                >
                                  <p>{message.content}</p>
                                </div>
                                <small className="mt-2 d-block text-black-50">
                                  <FontAwesomeIcon
                                    icon={["far", "clock"]}
                                    className="mr-1 opacity-5"
                                  />
                                  {renderTimestamp(message.timestamp)}
                                </small>
                              </div>
                            </div>
                          </div>
                        );
                      } else {
                        return (
                          <div key={message.id} className="chat-item p-2 mb-2">
                            <div className="align-box-row">
                              <div className="avatar-icon-wrapper avatar-icon-lg align-self-start">
                                <div className="avatar-icon rounded-circle shadow-none">
                                  <img alt="..." src={message.sender_img} />
                                </div>
                              </div>
                              <div>
                                <div className="chat-box" style={colorStyle}>
                                  <p>{message.content}</p>
                                </div>
                                <small className="mt-2 d-block text-black-50">
                                  <FontAwesomeIcon
                                    icon={["far", "clock"]}
                                    className="mr-1 opacity-5"
                                  />
                                  {renderTimestamp(message.timestamp)}
                                </small>
                              </div>
                            </div>
                          </div>
                        );
                      }
                    })}
                  </div>
                ) : (
                  <Card className="align-items-center justify-content-center font-weight-bold flex-wrap bg-white">
                    No messages so far
                  </Card>
                )}
              </div>
            )}
          </div>

          <div className="bg-white">
            <div className="card-footer p-0">
              <div
                className={clsx(
                  "d-flex align-items-center transition-base px-4 py-3",
                  { "bg-secondary": inputBg }
                )}
              >
                <div className="avatar-icon-wrapper avatar-initials avatar-icon-lg mr-3">
                  <div className="avatar-icon bg-neutral-dark text-black">
                    H
                  </div>
                  <Badge
                    color="success"
                    className="badge-position badge-position--bottom-center badge-circle"
                    title="Badge bottom center"
                  >
                    Online
                  </Badge>
                </div>

                <form
                  action="#"
                  onSubmit={sendMessageHandler}
                  style={{ width: "100%" }}
                  autoComplete="off"
                >
                  <Row>
                    <Col xs={11} xl={11} md={11}>
                      <Input
                        onFocus={toggleInputBg}
                        onBlur={toggleInputBg}
                        className={clsx(
                          "transition-base border-0 pl-2 font-size-lg",
                          { "pl-4": inputBg }
                        )}
                        bsSize="lg"
                        placeholder="Write your message here..."
                        onChange={messageChangeHandler}
                        value={message}
                      />
                    </Col>
                    <Col xs={1} md={1} xl={1}>
                      <Button
                        size="lg"
                        color="first"
                        className="btn-pill d-40 p-0 mt-2"
                        disabled={message === ""}
                      >
                        <FaPaperPlane />
                      </Button>
                    </Col>
                  </Row>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div
          className={clsx(
            "app-inner-content-layout--sidebar pos-r order-2 order-lg-3 bg-white app-inner-content-layout--sidebar__sm",
            { "layout-sidebar-open": isSidebarMenuOpen2 }
          )}
        >
          {props.user?.role == "PATIENT" && (
            <div>
              <div className="text-uppercase font-size-sm text-primary font-weight-bold my-3 px-3">
                Doctors
              </div>
              <PerfectScrollbar>
                <Nav className="nav-neutral-first flex-column">
                  {props.chats &&
                    props.chats.map((chat, index) => (
                      <NavItem key={index}>
                        <Button
                          className="hover-scale mb-2"
                          style={buttonStyle}
                        >
                          <Link
                            to={{
                              pathname: `/chat/${chat.id}`,
                              state: {
                                // doctor_last_name: chat.doctor_last_name,
                                doctor_username: chat.doctor_name,
                              },
                            }}
                          >
                            <div className="align-box-row">
                              <div className="avatar-icon-wrapper avatar-icon-sm">
                                <Badge color="warning" className="badge-circle">
                                  Idle
                                </Badge>
                                <div className="avatar-icon rounded-circle">
                                  <a href="https://imgbb.com/">
                                    <img alt="..." src={chat.doctor_image} />
                                  </a>
                                </div>
                              </div>
                              <div className="pl-2">
                                <span className="d-block text-black font-size-sm font-weight-bold">
                                  Dr. {chat.doctor_name}
                                </span>
                              </div>
                            </div>
                          </Link>
                        </Button>
                      </NavItem>
                    ))}
                </Nav>
              </PerfectScrollbar>
            </div>
          )}
          {props.user?.role == "DOCTOR" && (
            <div>
              <div className="text-uppercase font-size-sm text-primary font-weight-bold my-3 px-3">
                PATIENTS
              </div>
              <PerfectScrollbar>
                <Nav className="nav-neutral-first flex-column">
                  {props.chats &&
                    props.chats.map((chat) => (
                      <NavItem>
                        <Button
                          className="hover-scale mb-2"
                          style={buttonStyle}
                        >
                          <Link
                            to={{
                              pathname: `/chat/${chat.id}`,
                              state: {
                                patient_username: chat.patient_name,
                              },
                            }}
                          >
                            <div className="align-box-row">
                              <div className="avatar-icon-wrapper avatar-icon-sm">
                                <Badge color="warning" className="badge-circle">
                                  Idle
                                </Badge>
                                <div className="avatar-icon rounded-circle">
                                  <a href="https://imgbb.com/">
                                    <img alt="..." src={chat.patient_image} />
                                  </a>
                                </div>
                              </div>
                              <div className="pl-2">
                                <span className="d-block text-black font-size-sm font-weight-bold">
                                  Patient. {chat.patient_name}
                                </span>
                              </div>
                            </div>
                          </Link>
                        </Button>
                      </NavItem>
                    ))}
                </Nav>
              </PerfectScrollbar>
            </div>
          )}
        </div>

        {/* <div
          onClick={toggleSidebarMenu2}
          className={clsx("sidebar-inner-layout-overlay", {
            active: isSidebarMenuOpen2,
          })}
        /> */}
      </div>
    </React.Fragment>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.auth?.user,
    token: state.auth?.access,
    //username: state.auth.user?.username,
    tel: state.auth.user?.tel,
    chats: state.message.chats,
    messages: state.message.messages,
    loading: state.message.loading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getUserChats: (token) => dispatch(messageActions.getUserChats(token)),
    addMessage: (message) => dispatch(messageActions.addMessage(message)),
    setLoading: (loading) => dispatch(messageActions.setLoading(loading)),
    setMessages: (messages) => dispatch(messageActions.setMessages(messages)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);
