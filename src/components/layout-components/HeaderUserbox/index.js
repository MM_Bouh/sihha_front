import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  Badge,
  DropdownMenu,
  DropdownToggle,
  ListGroup,
  ListGroupItem,
  Nav,
  NavItem,
  NavLink as NavLinkStrap,
  UncontrolledDropdown,
} from "reactstrap";
import { logout } from "../../../actions/auth";

const HeaderUserbox = (props) => {
  return (
    <>
      <UncontrolledDropdown className="position-relative ml-2">
        <DropdownToggle
          color="link"
          className="p-0 text-left d-flex btn-transition-none align-items-center"
        >
          <div className="d-block p-0 avatar-icon-wrapper">
            <Badge color="success" className="badge-circle p-top-a">
              Online
            </Badge>
            <div className="avatar-icon rounded">
              <img
                // src={props.authenticated && props.auth.user.profile_img_url}
                src={props.auth.user?.profile_img_url}
                alt="..."
              />
            </div>
          </div>
          <div className="d-none d-xl-block pl-2">
            <div className="font-weight-bold">
              {props.authenticated && props.auth.user.last_name}
            </div>
          </div>
          <span className="pl-1 pl-xl-3">
            <FontAwesomeIcon
              icon={["fas", "angle-down"]}
              className="opacity-5"
            />
          </span>
        </DropdownToggle>
        <DropdownMenu right className="dropdown-menu-lg overflow-hidden p-0">
          <ListGroup flush className="text-left bg-transparent">
            <ListGroupItem className="rounded-top">
              <Nav pills className="nav-neutral-primary flex-column">
                <NavItem>
                  <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                    {/*IF ROLE == PATIENT ==> RENDER PROFILE PATIENT   |||   IF ROLE == DOCTOR ==> RENDER PROFILE DOCTOR*/}
                    {props.authenticated &&
                    props.auth.user.role === "PATIENT" ? (
                      <Link to="/Profile">My Account</Link>
                    ) : props.authenticated &&
                      props.auth.user.role === "DOCTOR" ? (
                      <Link to="/Profile-doctor">My Account</Link>
                    ) : (
                      <Link to="/Profile_agent">My Account</Link>
                    )}
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => {
                      e.preventDefault();
                      props.logout();
                    }}
                  >
                    Logout
                  </NavLinkStrap>
                </NavItem>
                <NavItem></NavItem>
              </Nav>
            </ListGroupItem>
          </ListGroup>
        </DropdownMenu>
      </UncontrolledDropdown>
    </>
  );
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  authenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps, { logout })(HeaderUserbox);
