import axios from "axios";
import React, { useEffect, useState } from "react";
import { Typeahead } from 'react-bootstrap-typeahead';
//Toaster Code 
import { toast } from "react-toastify";
import {
  Button, Col, FormFeedback, FormGroup,
  Input, Modal,
  Row
} from "reactstrap";
import SuccessSound from "../../assets/success-sound-effect.mp3";



const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};
const ModalDoctor = (props) => {
    const [username,setUsername] = useState("")
    const [email,setEmail] = useState("")
    const [lastname,setLastname] = useState("")
    const [firstname,setFirstname] = useState("")
    const [tel,setTel] = useState("")
    const [pass1,setPass1] = useState("")
    const [pass2,setPass2] = useState("")
    const [telExist,setTelExist] = useState("")
    const [pass1valid,setPass1valid] = useState("")
    const [pass2valid,setPass2valid] = useState("")

    const [experience,setExperience] = useState(0)
    const [specialities,setSpecialities] = useState([])
    const [specialitySelected, setSpecialitySelected] = useState("")
    //End my states
    useEffect(()=>{
      //Get the specialties list
      axios
          .get('http://localhost:8000/auth-api/specialities/')
          .then((res) => {
            setSpecialities(res.data)
            console.log(res.data)
          })
          .catch((err) => {
            console.log("Say Error")
          })
    },[])

  
  
  //Values For creations
  const handleSeletedSpeciality = (e) => {
      
    {e && e[0] && setSpecialitySelected(e[0].id)}
  }
  
  const experienceHandler = (e) =>{
    setExperience(e.target.value)
  }
    //Handlers and functions
    const usernameHandler = (e) => {
        setUsername(e.target.value)
    }
    const emailHandler = (e) => {
      setEmail(e.target.value)
    }
    const lastnameHandler = (e) => {
        setLastname(e.target.value)
    }
    const firstnameHandler = (e) => {
      setFirstname(e.target.value)
    }
    const telHandler = (e) => {
      setTelExist("")
      setTel(e.target.value)
    }
    const pass1Handler = (e) => {
      setPass1valid("")
      setPass2valid("")
      setPass1(e.target.value)
    }
    const pass2Handler = (e) => {
      setPass2valid("")
      setPass1valid("")
      setPass2(e.target.value)
    }
  
  
    const onSubmitHandler = (e) => {
      e.preventDefault();
      if(checkPasswords()){
  
          //config
          const config = {
              headers: {
              "Content-Type": "application/json",
              },
          };
          //body
          const body = {
              username:username,
              role:'DOCTOR',
              password:pass1,
              last_name:lastname,
              first_name:firstname,
              email:email,
              tel:tel,
              speciality:specialitySelected,
              experience: experience,
              profile_img_url:'https://i.ibb.co/kX1yFLF/avatar2.png',//CHANGE: you make it dynamic here , where the user choose the profile image
  
          };
          axios
              .post('http://localhost:8000/auth-api/create_doctor/',body,config)
              .then((res) => {
                  if(res.data.error){
                      setTelExist("invalid");
                  }else{
                    const sound = new Audio(SuccessSound);
                    sound.volume = 0.007;
                    sound.play();
                    successNotify("success", "Success Creating the doctor");
                    setTimeout(()=>{

                    },[3000]);
                    window.location = '/AdminConsultations'
                  }
              })
      }else{
          setPass1valid("invalid")
          setPass2valid("invalid")
      }
    }
  
    const checkPasswords = () => {
  
        //Check if the given passwords are equal and have a length gte 6
        if((pass1===pass2)&&(pass1.length >= 6)&&(pass2.length >= 6)){
            return true
        }else{
            return false
        }
  
    }
    const cancelHandler = () => {
        window.location ='/AdminConsultations'
    }
    return (
      <>
        <div className="d-flex align-items-center justify-content-center flex-wrap">
  
  
         
          <Modal
            zIndex={2000}
            centered
            size="lg"
            isOpen={true}
            toggle={true}
            contentClassName="border-0">
           <div className="text-center p-5">
              
           
           <Col md="12" lg="12" xl="12" className="mx-auto">
                    <div className="text-center mb-4">
                      <h1 className="display-4 mb-1 font-weight-bold">
                        Create Doctor Account
                      </h1>
                    </div>
                    <form onSubmit={onSubmitHandler}>
                    <FormGroup >
                    <div className="d-flex justify-content-between mg-b-5">
                        <label className="font-weight-bold mb-0">Telephone</label>
                      </div>
                      <Input
                        placeholder="Enter your telephone number ex. 32341233"
                        type="telephone"
                        onChange = {telHandler}
                        className="mb-3"
                        required="required"
                        invalid={telExist}
                      />
                      {telExist !== 'invalid' ? '':<FormFeedback>Telephone Number already exist !!!</FormFeedback>}{/*CHECK IF TEL NUMBER ALREADY EXISTS*/}
                      <div className="d-flex justify-content-between mg-b-5">
                      <label className="font-weight-bold mb-0">Username</label>
                      </div>
                      <Input
                        placeholder="Enter your username"
                        type="text"
                        onChange = {usernameHandler} 
                        className="mb-3"
                      />
                      
                    <div className="d-flex justify-content-between mg-b-5">
                    <label className="font-weight-bold mb-0">Email address</label>
                    </div>
                      <Input
                        placeholder="Enter your email address"
                        type="email"
                        onChange = {emailHandler}
                        className="mb-3"
                      />
                      
                    
                    <Row>
                      <Col md="6">
                        
                          <label className="font-weight-bold mb-0">First name</label>
                          <Input
                            placeholder="Enter your firstname"
                            type="text"
                            onChange = {firstnameHandler}
                            className="mb-3"
                          />
                       
                      </Col>
                      <Col md="6">
                        
                          <label className="font-weight-bold mb-0">Last name</label>
                          <Input
                            placeholder="Enter your lastname"
                            type="text"
                            onChange = {lastnameHandler}
                            className="mb-3"
                          />
                       
                      </Col>
                    </Row>
                   
                    <Row>
                      <Col md="6">
                        
                          <label className="font-weight-bold mb-0">Speciality</label>
                          <Typeahead
                            id="typeID2"
                            labelKey="name"
                            multiple={false}
                            options={specialities && specialities}
                            placeholder="Choose a speciality ..."
                            onChange = {handleSeletedSpeciality}
                            
                          />
                       
                      </Col>
                      <Col md="6">
                        
                          <label className="font-weight-bold mb-0">Experience</label>
                          <Input
                            placeholder="His experience in years"
                            type="number"
                            onChange = {experienceHandler}
                            className="mb-3"
                          />
                       
                      </Col>
                    </Row>


                    
                      <div className="d-flex justify-content-between mg-b-5">
                        <label className="font-weight-bold mb-0">Password</label>
                      </div>
                      <Input
                        placeholder="Enter your password at least 6 characters"
                        type="password"
                        onChange = {pass1Handler}
                        required="required"
                        invalid={pass1valid}
                        className="mb-3"
                      />
                      <div className="d-flex justify-content-between mg-b-5">
                        <label className="font-weight-bold mb-0">Confirm Password</label>
                      </div>
                      <Input
                        placeholder="Enter your password again"
                        type="password"
                        onChange = {pass2Handler}
                        required="required"
                        invalid = {pass2valid}
                        className="mb-3"
                      />
                            
                    <div className="text-center mb-3">
                      <Button
                        color="primary"
                        className="text-uppercase font-weight-bold font-size-sm my-3 mr-3"  type="submit" onclick={onSubmitHandler}>
                        Create account
                      </Button>
                      <Button
                        onClick={cancelHandler}
                        color="secondary"
                        className="text-uppercase font-weight-bold font-size-sm my-3">
                        Cancel
                      </Button>                     
                    </div>
                    </FormGroup>  
                    </form>
                  </Col>
            </div>
          </Modal>
        </div>
      </>
    );
  }

  export default ModalDoctor;