import React, { useEffect } from "react";
import { Box, MessageCircle } from "react-feather";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Badge, Button, Card, CardBody, Table } from "reactstrap";
import * as messageActions from "../../../actions/message";
import WebSocketInstance from "../../../websocket";

function LivePreviewExample(props) {
  useEffect(() => {
    props.getUserChats(props.token);

    props.getUserChats(props.token);

    WebSocketInstance.addCallbacks(props.setMessages, props.addMessage);
  }, []);

  //STATES
  //const [patients, setPatients] = useState([]);

  return (
    <>
      <Card className="card-box mb-5">
        <div className="card-header pr-2">
          <div className="card-header--title">Patients</div>
        </div>
        <CardBody>
          <div className="table-responsive-md">
            <Table hover borderless className="text-nowrap mb-0">
              <thead>
                <tr>
                  <th className="text-left">Patient</th>
                  <th className="text-center">Consultation State</th>
                  <th className="text-center">Consultation</th>
                  <th className="text-center">Medical Record</th>
                </tr>
              </thead>
              <tbody>
                {/* MAP OVER THE CHATS */}

                {props.chats.map((chat, index) => (
                  <tr key={index}>
                    <td>
                      <div className="d-flex align-items-center">
                        <div className="avatar-icon-wrapper mr-3">
                          <div className="avatar-icon">
                            <img alt="..." src={chat.patient_image} />
                          </div>
                        </div>
                        <div>
                          <a
                            href="#/"
                            onClick={(e) => e.preventDefault()}
                            className="font-weight-bold text-black"
                            title="..."
                          >
                            {chat.patient_name}
                          </a>
                        </div>
                      </div>
                    </td>

                    <td className="text-center">
                      <Badge
                        color={
                          chat.state === "PENDING"
                            ? "warning"
                            : chat.state === "ACTIVE"
                            ? "success"
                            : "first"
                        }
                        className="h-auto py-0 px-3"
                      >
                        {chat.state}
                      </Badge>
                    </td>

                    <td className="text-center">
                      <Button
                        size="sm"
                        color="neutral-dark"
                        className="hover-scale-sm d-40 p-0 btn-icon"
                      >
                        <span className="sidebar-icon">
                          {/* <Link to={`/chat/${chat.id}`}> */}
                          <Link
                            to={{
                              pathname: `/chat/${chat.id}`,
                              state: {
                                // patient_last_name: chat.patient_last_name,
                                patient_username: chat.patient_name,
                              },
                            }}
                          >
                            <MessageCircle />
                          </Link>
                        </span>
                      </Button>
                    </td>
                    <td className="text-center">
                      <Button
                        size="sm"
                        color="neutral-dark"
                        className="hover-scale-sm d-40 p-0 btn-icon"
                      >
                        <span className="sidebar-icon">
                          <Link
                            to={{
                              pathname: `/MedicalRecord_patient/${chat.patient}`,
                              state: {
                                id_patient: chat.patient,
                                name: chat.patient_first_name,
                                surname: chat.patient_last_name,
                              },
                            }}
                          >
                            <Box />
                          </Link>
                          {/* <Link to={"/MedicalRecord_patient/"+patient.id}><Box /></Link> */}
                        </span>
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </CardBody>
      </Card>
    </>
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  token: state.auth.access,
  // username: state.auth.user.username,
  chats: state.message.chats,
});
const mapDispatchToProps = (dispatch) => {
  return {
    getUserChats: (token) => dispatch(messageActions.getUserChats(token)),
    addMessage: (message) => dispatch(messageActions.addMessage(message)),
    setMessages: (messages) => dispatch(messageActions.setMessages(messages)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LivePreviewExample);
