import React from "react";
import AvailableDoctors from "./available_doctors";
import AVAILABLESPE from "./available_specialities";

// import Projects from './Slider'
function home() {
  return (
    <div>
      <AvailableDoctors />
      <AVAILABLESPE />
    </div>
  );
}

export default home;
