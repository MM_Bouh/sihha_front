import axios from "axios";
import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";

import { Row, Col, FormGroup, Input, Button } from "reactstrap";
import FormFeedback from "reactstrap/lib/FormFeedback";

import { toast } from "react-toastify";
// import PhoneInput from 'react-phone-number-input'
const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};

export default function LivePreviewExample() {
  const history = useHistory();

  //My states
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [lastname, setLastname] = useState("");
  const [firstname, setFirstname] = useState("");
  const [tel, setTel] = useState("");
  const [pass1, setPass1] = useState("");
  const [pass2, setPass2] = useState("");
  const [telExist, setTelExist] = useState("");
  const [pass1valid, setPass1valid] = useState("");
  const [pass2valid, setPass2valid] = useState("");
  //End my states

  //Handlers and functions
  const usernameHandler = (e) => {
    setUsername(e.target.value);
  };
  const emailHandler = (e) => {
    setEmail(e.target.value);
  };
  const lastnameHandler = (e) => {
    setLastname(e.target.value);
  };
  const firstnameHandler = (e) => {
    setFirstname(e.target.value);
  };
  const telHandler = (e) => {
    setTelExist("");
    setTel(e.target.value);
  };
  const pass1Handler = (e) => {
    setPass1valid("");
    setPass2valid("");
    setPass1(e.target.value);
  };
  const pass2Handler = (e) => {
    setPass2valid("");
    setPass1valid("");
    setPass2(e.target.value);
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();
    if (checkPasswords()) {
      //config
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      //body
      const body = {
        username: username,
        role: "PATIENT",
        password: pass1,
        last_name: lastname,
        first_name: firstname,
        email: email,
        tel: tel,
        profile_img_url: "https://i.ibb.co/kX1yFLF/avatar2.png", //CHANGE: you make it dynamic here , where the user choose the profile image
      };
      axios
        // .post('https://medicare-back.herokuapp.com/auth-api/create_user/',body,config)
        .post("http://localhost:8000/auth-api/create_patient/", body, config)
        .then((res) => {
          if (res.data.error) {
            setTelExist("invalid");
          } else {
            successNotify(
              "success",
              "Congrats, Your account has been successfully created"
            );
            history.push("/Login");
            //console.log(res.data);
          }
        });
    } else {
      setPass1valid("invalid");
      setPass2valid("invalid");
    }
  };
  const checkPasswords = () => {
    //Check if the given passwords are equal and have a length gte 6
    if (pass1 === pass2 && pass1.length >= 6 && pass2.length >= 6) {
      return true;
    } else {
      return false;
    }
  };
  //End Handlers and functions

  return (
    <>
      <div className="app-wrapper bg-white min-vh-100">
        <div className="app-main min-vh-100">
          <div className="app-content p-0">
            <div className="app-content--inner d-flex align-items-center">
              <div className="flex-grow-1 w-100 d-flex align-items-center">
                <div className="bg-composed-wrapper--content py-5">
                  <Col md="10" lg="8" xl="4" className="mx-auto">
                    <div className="text-center mb-4">
                      <h1 className="display-4 mb-1 font-weight-bold">
                        Create your account
                      </h1>
                    </div>
                    <form onSubmit={onSubmitHandler}>
                      <FormGroup>
                        <div className="d-flex justify-content-between mg-b-5">
                          <label className="font-weight-bold mb-0">
                            Telephone
                          </label>
                        </div>
                        <Input
                          placeholder="Enter your telephone number ex. 32341233"
                          type="number"
                          onChange={telHandler}
                          className="mb-3"
                          required="required"
                          invalid={telExist}
                        />
                        {telExist !== "invalid" ? (
                          ""
                        ) : (
                          <FormFeedback>
                            Telephone Number already exists !!!
                          </FormFeedback>
                        )}
                        {/*CHECK IF TEL NUMBER ALREADY EXISTS*/}
                        <label className="font-weight-bold mb-0">
                          Username
                        </label>
                        <Input
                          placeholder="Enter your username"
                          type="text"
                          onChange={usernameHandler}
                          className="mb-3"
                        />

                        <label className="font-weight-bold mb-0">
                          Email address
                        </label>
                        <Input
                          placeholder="Enter your email address"
                          type="email"
                          onChange={emailHandler}
                          className="mb-3"
                        />

                        <Row>
                          <Col md="6">
                            <label className="font-weight-bold mb-0">
                              First name
                            </label>
                            <Input
                              placeholder="Enter your firstname"
                              type="text"
                              onChange={firstnameHandler}
                              className="mb-3"
                            />
                          </Col>
                          <Col md="6">
                            <label className="font-weight-bold mb-0">
                              Last name
                            </label>
                            <Input
                              placeholder="Enter your lastname"
                              type="text"
                              onChange={lastnameHandler}
                              className="mb-3"
                            />
                          </Col>
                        </Row>

                        <div className="d-flex justify-content-between mg-b-5">
                          <label className="font-weight-bold mb-0">
                            Password
                          </label>
                        </div>
                        <Input
                          placeholder="Enter your password at least 6 characters"
                          type="password"
                          onChange={pass1Handler}
                          required="required"
                          invalid={pass1valid}
                          className="mb-3"
                        />

                        <div className="d-flex justify-content-between mg-b-5">
                          <label className="font-weight-bold mb-0">
                            Confirm Password
                          </label>
                        </div>
                        <Input
                          placeholder="Enter your password again"
                          type="password"
                          onChange={pass2Handler}
                          required="required"
                          invalid={pass2valid}
                          className="mb-3"
                        />
                        {pass2valid !== "invalid" ||
                        pass1valid !== "invalid" ? (
                          ""
                        ) : (
                          <FormFeedback>
                            Passwords do not match or the length is less than 6
                            !!
                          </FormFeedback>
                        )}
                        {/*CHECK If the passwords match */}
                        <div className="text-center mb-3">
                          <Button
                            color="primary"
                            className="text-uppercase font-weight-bold font-size-sm my-3"
                            type="submit"
                          >
                            Create account
                          </Button>
                        </div>
                      </FormGroup>
                    </form>
                    <div className="text-center mb-5">
                      <Button>
                        <Link
                          to="/login"
                          color="primary"
                          className="text-uppercase font-weight-bold font-size-sm my-3"
                        >
                          Back to login
                        </Link>
                      </Button>
                    </div>
                  </Col>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
