import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import clsx from "clsx";
import React, { Component, useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
//import { saveAs } from "file-saver";
import { toast } from "react-toastify";
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Collapse,
  Container,
  CustomInput,
  FormGroup,
  Input,
  Label,
  ListGroup,
  ListGroupItem,
  Row,
} from "reactstrap";
import useFileDownloader from "../../../hooks/useFileDownloader";

const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};
const MedicalQuestionnaire = (props) => {
  const [answers, setAnswers] = useState([]);
  useEffect(() => {
    setAnswers(props.answers);
  });

  return (
    <>
      {" "}
      <div className="p-4">
        <fieldset disabled="disabled">
          {answers &&
            props.questions &&
            props.questions.map((question, i) => (
              <Card className="card-box mb-5" key={question.id}>
                <CardBody>
                  <CardTitle className="font-weight-bold font-size-lg mb-4">
                    {question.content}
                  </CardTitle>
                  <FormGroup>
                    <div>
                      <CustomInput
                        className="mb-3"
                        type="radio"
                        id={`1_${question.id}`}
                        name={`${question.id}`}
                        label="Yes"
                        checked={
                          answers && answers[i] && answers[i].choice === true
                        }
                      />
                      <CustomInput
                        className="mb-3"
                        type="radio"
                        id={`2_${question.id}`}
                        name={`${question.id}`}
                        label="No"
                        checked={
                          answers && answers[i] && answers[i].choice === false
                        }
                      />
                    </div>
                    {question.has_details && (
                      <>
                        <Label htmlFor="exampleText">
                          If yes, Please provide details:
                        </Label>
                        <Input
                          type="textarea"
                          name={`${question.id}`}
                          id="exampleText"
                          key={question.id}
                          defaultValue={
                            props.answers &&
                            props.answers[i] &&
                            props.answers[i].answer
                          }
                        />
                      </>
                    )}
                  </FormGroup>
                </CardBody>
              </Card>
            ))}
        </fieldset>
      </div>
    </>
  );
};

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const MedicalRecordFiles = (props) => {
  const previousLoading = usePrevious(props.loading);
  const [files, setFiles] = useState([]);

  useEffect(() => {
    if (props.loading === false && previousLoading) {
      setFiles(props.files);
    }
  });
  const [downloadFile, downloaderComponentUI] = useFileDownloader();

  const localhost = "http://localhost:8000";
  const download = (file) => {
    console.log("The file name", file);
    downloadFile(file);
  };
  const buttonStyle = {
    width: "80%",
  };

  const listFiles = files.map((file, i) => (
    <Row xs={1} md={1} lg={1} key={i}>
      <ListGroupItem
        className="font-size-sm px-3 py-2 text-primary d-flex justify-content-between align-items-center"
        key={i}
      >
        <Col xs={6} md={6} lg={6}>
          {file.id ? (
            <div>
              <span>
                <a
                  className="hover-scale mb-2 cursor-pointer"
                  style={buttonStyle}
                  onClick={() =>
                    download({
                      file:
                        "http://localhost:8000" +
                        file.medical_file +
                        "?rend=" +
                        Math.random(),
                    })
                  }
                >
                  {file.medical_file.split("/")[3]} {""}
                  {/* <FontAwesomeIcon icon="download" /> */}
                </a>
              </span>
            </div>
          ) : (
            <div>
              <span>
                <a href={localhost + file.name} target="_self" download={false}>
                  {file.name}
                </a>
                Download <FontAwesomeIcon icon="download" />
              </span>
            </div>
          )}{" "}
        </Col>
        {/* <Col xs={6} md={6} lg={6}>
          {downloaderComponentUI}
        </Col> */}
      </ListGroupItem>
    </Row>
  ));

  //RETURN
  return (
    <>
      <div>
        <div className="font-weight-bold my-4 text-uppercase text-dark font-size-sm text-center">
          Uploaded Files
        </div>
        {listFiles.length <= 0 && (
          <div className="text-info text-center font-size-sm"></div>
        )}
        {listFiles.length > 0 && (
          <div>
            <Alert color="success" className="text-center mb-3">
              {listFiles.length === 1 ? (
                <b>{listFiles.length} File!</b>
              ) : (
                <b>{listFiles.length} Files!</b>
              )}
            </Alert>

            {/*HERE I WILL CREATE A CONTAINER TO ORGANIZE THE LISTING OF FILES */}

            <Container fluid>
              <ListGroup className="font-size-sm">{listFiles}</ListGroup>
            </Container>
            <Col xs={6} md={6} lg={6}>
              {downloaderComponentUI}
            </Col>
          </div>
        )}
      </div>
    </>
  );
};

class LivePreviewExample extends Component {
  constructor(props) {
    super(props);
    this.toggleAccordion = this.toggleAccordion.bind(this);
    this.state = {
      patient: this.props.match.params.id,
      medical_record_id: "",
      accordion: [true, false, false],
      name: props.location.state.name,
      surname: props.location.state.surname,
      gender: "",
      height: null,
      weight: null,
      bloodType: "",
      age: null,
      questions: undefined,
      answers: undefined,
      files: [],
      loading: undefined,
    };
  }
  componentDidMount() {
    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_basic_info/" +
        "http://localhost:8000/medical-record-api/update_medical_record_basic_info/" +
          this.state.patient
      )
      .then((res) => {
        this.setState({
          ...this.state,
          medical_record_id: res.data.id,
          age: res.data.age,
          bloodType: res.data.blood_type,
          height: res.data.height,
          weight: res.data.weight,
          gender: res.data.gender,
        });
      })
      .catch((err) => {
        console.log(err);
      });

    // To get the questions
    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/get_questions_questionnaire/"
        "http://localhost:8000/medical-record-api/get_questions_questionnaire/"
      )
      .then((res) => {
        this.setState({ questions: res.data });
      })
      .catch((err) => {
        console.log(err);
      });

    // To get the answers
    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_questionnaire/" +
        "http://localhost:8000/medical-record-api/update_medical_record_questionnaire/" +
          this.state.patient
      )
      .then((res) => {
        this.setState({ ...this.state, answers: res.data });
      })
      .catch((err) => {
        console.log("Hello Error ", err);
      });

    this.setState({ ...this.state, loading: true });

    //console.log(this.props.history.location);
  }
  toggleAccordion(tab) {
    const prevState = this.state.accordion;
    const state = prevState.map((x, index) => (tab === index ? !x : false));
    if (state[2] == true) {
      axios
        .get(
          "http://localhost:8000/medical-record-api/medical_files/" +
            this.state.medical_record_id
          // {
          //   responseType: "blob",
          // }
        )
        .then((res) => {
          this.setState({ ...this.state, files: res.data, loading: false });
          console.log("The list of the files ", res.data);
        })
        .catch((err) => {
          this.setState({ ...this.state, loading: false });
          console.log("Hello Errors");
        });
    }
    this.setState({
      accordion: state,
    });
  }

  render() {
    return (
      <>
        <div className="accordion mb-5">
          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[0],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(0)}
                      aria-expanded={this.state.accordion[0]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        Personal information
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>

              <fieldset disabled="disabled">
                <Collapse isOpen={this.state.accordion[0]}>
                  <div className="p-4">
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label htmlFor="name">Name</label>
                        <input
                          className="form-control"
                          id="name"
                          placeholder="name"
                          type="text"
                          disabled={true}
                          value={this.state.name}
                        />
                        <label htmlFor="surname">Surname</label>
                        <input
                          className="form-control"
                          id="surname"
                          placeholder="Surname"
                          type="text"
                          disabled={true}
                          value={this.state.surname}
                        />
                        <label htmlFor="age">Age</label>
                        <input
                          className="form-control"
                          id="age"
                          placeholder="Age"
                          type="number"
                          disabled={true}
                          value={this.state.age}
                        />
                        <Label htmlFor="exampleCustomSelect">Gender</Label>
                        <CustomInput
                          type="select"
                          id="exampleCustomSelect"
                          name="customSelect"
                          value={this.state.gender}
                        >
                          <option>male</option>
                          <option>female</option>
                        </CustomInput>
                      </div>
                      <div className="form-group col-md-6">
                        <label htmlFor="height">Height</label>
                        <input
                          className="form-control"
                          id="height"
                          placeholder="Height in cm"
                          type="number"
                          value={this.state.height}
                        />
                        <label htmlFor="weight">Weight</label>
                        <input
                          className="form-control"
                          id="weight"
                          placeholder="Weight in kg"
                          type="number"
                          value={this.state.weight}
                        />
                        <Label htmlFor="blood-type">Blood Type</Label>
                        <CustomInput
                          type="select"
                          id="exampleCustomSelect"
                          name="customSelect"
                          value={this.state.bloodType}
                        >
                          <option></option>
                          <option>A+</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B-</option>
                          <option>AB+</option>
                          <option>AB-</option>
                          <option>O+</option>
                          <option>O-</option>
                        </CustomInput>
                      </div>
                    </div>
                  </div>
                </Collapse>
              </fieldset>
            </Card>
          </Card>
          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[1],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(1)}
                      aria-expanded={this.state.accordion[1]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        Medical Questionnaire
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>
              <Collapse isOpen={this.state.accordion[1]}>
                {/* Medical Questinnaire */}

                <MedicalQuestionnaire
                  questions={this.state.questions}
                  answers={this.state.answers}
                />
              </Collapse>
            </Card>
          </Card>

          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[2],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(2)}
                      aria-expanded={this.state.accordion[2]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        Medical Files
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>
              <Collapse isOpen={this.state.accordion[2]}>
                <div className="p-4">
                  <MedicalRecordFiles
                    files={this.state.files}
                    loading={this.state.loading}
                    medical_record_id={this.state.medical_record_id}
                  />
                </div>
              </Collapse>
            </Card>
          </Card>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(LivePreviewExample);
