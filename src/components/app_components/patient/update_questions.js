import React, { Component, useEffect, useRef, useState } from "react";
import { IconContext } from "react-icons";
import {
  Card,
  CardHeader,
  Collapse,
  Button,
  Label,
  CustomInput,
  CardBody,
  CardTitle,
  FormGroup,
  Input,
} from "reactstrap";

import { useDropzone } from "react-dropzone";

import { UploadCloud, Check, X } from "react-feather";

import { connect } from "react-redux";
import axios from "axios";

import { toast } from "react-toastify";

import SuccessSound from "../../assets/success-sound-effect.mp3";


const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};

const MedicalQuestionnaire = (props) => {
    const [answers, setAnswers] = useState([]);
    useEffect(() => {
      setAnswers(props.answers);
    });
    const handleChoise = (e) => {
      let question_id = e.target.name;
      if(answers && answers[question_id -1]){}
      let choice = answers[question_id - 1].choice;
      let answer = "";
      if (e.target.id === "exampleText") {
        answer = e.target.value;
      }
      if (e.target.id === `1_${question_id}`) {
        choice = true;
      } else if (e.target.id === `2_${question_id}`) {
        choice = false;
      }
  
      const newArray = answers.slice(); //copy the array
  
      newArray[question_id - 1] = {
        id: answers[question_id - 1].id,
        answer: answer,
        choice: choice,
        question: answers[question_id - 1].question,
        medical_record: answers[question_id - 1].medical_record,
      };
      setAnswers(newArray);
      props.onSelectAnswer(newArray, false);
    };
  
    const handleFinishAnswers = () => {
      props.onSelectAnswer(answers, true);
    };
  
    return (
      <>
        {" "}
        <div className="p-4">
          {answers &&
            props.questions &&
            props.questions.map((question, i) => (
              <Card className="card-box mb-5" key={question.id}>
                <CardBody>
                  <CardTitle className="font-weight-bold font-size-lg mb-4">
                    {question.content}
                  </CardTitle>
                  <FormGroup onChange={handleChoise}>
                    <div>
                      <CustomInput
                        className="mb-3"
                        type="radio"
                        id={`1_${question.id}`}
                        name={`${question.id}`}
                        label="Yes"
                        checked={answers && answers[i] && answers[i].choice === true}
                      />
                      <CustomInput
                        className="mb-3"
                        type="radio"
                        id={`2_${question.id}`}
                        name={`${question.id}`}
                        label="No"
                        checked={answers && answers[i] && answers[i].choice === false}
                      />
                    </div>
                    {question.has_details && (
                      <>
                        <Label htmlFor="exampleText">
                          If yes, Please provide details:
                        </Label>
                        <Input
                          type="textarea"
                          name={`${question.id}`}
                          id="exampleText"
                          key={question.id}
                          defaultValue={props.answers && props.answers[i] && props.answers[i].answer}
                        />
                      </>
                    )}
                  </FormGroup>
                </CardBody>
              </Card>
            ))}
  
          <div className="card-box mb-2">
          {/* <Button color="link" className="btn-link-dark" onClick={props.toggleModal()}>
              Close
          </Button>{' '} */}
            <Button
              type="submit"
              color="primary"
              // className="hover-scale-sm font-weight-bold  px-4 "
              className="ml-auto"
              onClick={handleFinishAnswers}
            >Save Changes</Button>
          </div>
        </div>
      </>
    );
  };

class UpdateQuestions extends Component {
  
    constructor(props) {
      super(props);
      this.state = {
        questions: undefined,
        answers: undefined,
        loading: undefined,
        
      };
      
    }
  
    componentDidMount() {
      
      axios
        .get(
          // "https://medicare-back.herokuapp.com/medical-record-api/get_questions_questionnaire/"
          "http://localhost:8000/medical-record-api/get_questions_questionnaire/"
        )
        .then((res) => {
          this.setState({ questions: res.data });
        })
        .catch((err) => {
        });
  
        
        axios
          .get(
            // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_questionnaire/" +
            "http://localhost:8000/medical-record-api/update_medical_record_questionnaire/" +
              this.props.auth.user.id
          )
          .then((res) => {
            this.setState({ ...this.state, answers: res.data });
          })
          .catch((err) => {
            
          });
  
      this.setState({ ...this.state, loading: true }); 
        
    }
  
  
    render() {
      
      //Function to Fill the answers comming from questions
  
      const handleAnswers = (answers, submit) => {
        this.setState({ answers: answers });
        if (submit) {
          axios
            .put(
              "http://localhost:8000/medical-record-api/update_medical_record_questionnaire/" +
                this.props.auth.user.id,
              {
                answers: this.state.answers,
              }
            )
            .then((response) => {
              const sound = new Audio(SuccessSound);
              sound.volume = 0.007;
              sound.play();
              successNotify(
                "success",
                "Success Submitting Medical Questionnaire"
              );
            })
            .catch((err) => {
              successNotify("fail", "Failed Submitting Medical Questionnaire");
            });
        }
      };
  
      return (
        <>
          <MedicalQuestionnaire
                questions={this.state.questions}
                onSelectAnswer={handleAnswers}
                answers={this.state.answers}
                                    // toggleModal={this.props.toggleModal}
           />
        </>
      );
    }
  }

  const mapStateToProps = (state) => ({
    auth: state.auth,
  });
  
  export default connect(mapStateToProps)(UpdateQuestions);