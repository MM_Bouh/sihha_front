import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import {
  ButtonDropdown,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  ListGroup,
  ListGroupItem,
  Nav,
  NavItem,
  NavLink as NavLinkStrap,
  DropdownButton,
  ButtonGroup,
  Button,
} from "reactstrap";

const Sort = ({ headers, onSorting }) => {
  const [sortingField, setSortingField] = useState("");
  const [sortingOrder, setSortingOrder] = useState("asc");

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const onMouseEnter = () => {
    //this.setState({ dropdownOpen: true });
    setDropdownOpen(true);
  };

  const onMouseLeave = () => {
    // this.setState({ dropdownOpen: false });
    setDropdownOpen(false);
  };
  const writeStyle = {
    color: "#5A20CB",
  };

  const onSortingChange = (field) => {
    const order =
      field === sortingField && sortingOrder === "asc" ? "desc" : "asc";

    setSortingField(field);
    setSortingOrder(order);
    onSorting(field, order);
  };

  return (
    <React.Fragment>
      <Dropdown
        direction="right"
        as={ButtonGroup}
        onMouseOver={onMouseEnter}
        onMouseLeave={onMouseLeave}
        isOpen={dropdownOpen}
        toggle={toggle}
        className="position-relative ml-2"
      >
        <DropdownToggle
          color="link"
          // className="p-0 text-left d-flex btn-transition-none align-items-center"
          className="p-0 text-left align-items-center"
        >
          <div className="">
            <div className="font-size-xl font-weight-bold pt-1 pb-3 text-center">
              Sort By{" "}
              <span className="icon-wrapper opacity-5 dropdown-arrow">
                <FontAwesomeIcon
                  icon={
                    dropdownOpen ? ["fas", "angle-up"] : ["fas", "angle-down"]
                  }
                  size="lg"
                />
              </span>
            </div>
          </div>
        </DropdownToggle>
        <DropdownMenu
          left
          //onMouseLeave={onMouseLeave}
          className="dropdown-menu-lg overflow-hidden m-0 p-0"
        >
          <ListGroup flush className="text-left bg-transparent">
            <ListGroupItem className="rounded-top">
              <Nav pills className="nav-neutral-primary flex-column">
                {headers.map(({ name, field, sortable }) => (
                  <NavItem key={name}>
                    <NavLinkStrap
                      key={name}
                      onClick={() => (sortable ? onSortingChange(field) : null)}
                      style={writeStyle}
                    >
                      {name} {"  "}
                      {sortingField && sortingField === field && (
                        <FontAwesomeIcon
                          icon={
                            sortingOrder === "asc" ? "arrow-down" : "arrow-up"
                          }
                        />
                      )}
                    </NavLinkStrap>
                  </NavItem>
                ))}
              </Nav>
            </ListGroupItem>
          </ListGroup>
        </DropdownMenu>
      </Dropdown>

      {/* <div className="header-nav-wrapper header-nav-wrapper-lg navbar-dark">
        <div className="header-nav-menu d-none d-lg-block">
          <ul className="d-flex nav nav-neutral-first justify-content-center">
            <li>
              <a
                href="#/"
                onClick={(e) => e.preventDefault()}
                className="font-weight-bold rounded-lg text-black"
              >
                Apps
                <span className="opacity-5 dropdown-arrow">
                  <FontAwesomeIcon icon={["fas", "angle-down"]} />
                </span>
              </a>
              <div className="submenu-dropdown submenu-dropdown--xl">
                <Row className="no-gutters">
                  <Col lg="7" className="z-over">
                    <div className="shadow-sm-dark w-100 bg-white p-3 rounded">
                      <div className="px-4 text-uppercase pb-2 text-primary font-weight-bold font-size-sm">
                        Dashboards
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </li>
          </ul>
        </div>
      </div> */}
    </React.Fragment>
  );
};
// Dropdown.propTypes = {
//   className: PropTypes.string,
//   disabled: PropTypes.bool,
//   direction: PropTypes.oneOf(["up", "down", "left", "right"]),
//   isOpen: PropTypes.bool,
//   // For Dropdown usage inside a Nav
//   nav: PropTypes.bool,
//   active: PropTypes.bool,
//   // For Dropdown usage inside a Navbar (disables popper)
//   inNavbar: PropTypes.bool,
//   tag: PropTypes.string, // default: 'div' unless nav=true, then 'li'
//   toggle: PropTypes.func,
//   setActiveFromChild: PropTypes.bool,
// };

export default Sort;
