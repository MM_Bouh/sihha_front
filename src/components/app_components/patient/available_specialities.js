import React, { useEffect, useState } from "react";
import ReactCardFlip from "react-card-flip";
import { connect } from "react-redux";
import { Heart } from "react-spinners-css";
import { Card, CardText, CardTitle, Col, Row } from "reactstrap";
import * as specialityActions from "../../../actions/specialityActions";
import { host } from "../../../actions/type";
import Message from "../Message";

function AVAILABLESPE(props) {
  const specialityList = props.specialities;
  const { loading, error, specialities } = specialityList;

  const SuspenseLoading = () => {
    return (
      <>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <Heart color={"#3c44b1"} />
          </div>
        </div>
      </>
    );
  };

  useEffect(() => {
    //Get the specialties list
    props.listSpecialities();
  }, []);
  const writeStyle = {
    color: "#5A20CB",
    text: "center",
    margin: "0.5em",
    padding: "0.5em",
    textAlign: "center",
    // textAlign: 'center', // <-- the magic
    // fontWeight: 'bold',
    // fontSize: 18,
    // marginTop: 0,
    // width: 200,
    // backgroundColor: 'yellow',
  };
  const titleStyle = {
    color: "#1B98F5",
    text: "center",
  };
  const cardStyle = {
    // transition: "all 1s ease-in",
    width: "100",
    height: "100",
    justifyContent: "center",
    alignItems: "center",
  };
  const CardF = ({ speciality }) => {
    const [isFlipped, setIsFlipped] = useState(false);

    return (
      <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
        {/* The front of the Card */}

        <Card
          // style={cardStyle}
          onMouseEnter={() => setIsFlipped((prev) => !prev)}
          className="CardFront card-box mb-5 hover-scale-rounded"
        >
          <div>
            <div className="text-center py-3">
              <div className="d-flex align-items-center pb-4 justify-content-between">
                <div className="d-90 rounded-circle border-0 my-2 card-icon-wrapper bg-plum-plate btn-icon mx-auto text-center">
                  <div className="avatar-icon-wrapper d-80">
                    <div className="avatar-icon d-80 rounded-circle">
                      {/* <img alt="..." src={speciality.image} /> */}
                      <img src={host + speciality.image} alt="..." />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Card>
        {/* The back of the Card */}
        <Card
          style={cardStyle}
          onMouseLeave={() => setIsFlipped((prev) => !prev)}
          className="CardBack"
        >
          <div>
            <CardTitle className="font-weight-bold" style={writeStyle}>
              {speciality.name}
            </CardTitle>
            <CardText className="text-black-50 d-block" style={writeStyle}>
              {speciality.description}
            </CardText>
          </div>
        </Card>
      </ReactCardFlip>
    );
  };
  return (
    <div>
      <div
        className="font-size-xl font-weight-bold pt-3 pb-3 text-center mt-5"
        style={titleStyle}
      >
        AVAILABLE SPECIALITIES
      </div>
      {loading ? (
        <SuspenseLoading />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Row>
          {specialities &&
            specialities[0] &&
            specialities.map((speciality, index) => (
              <Col md="4" key={index}>
                <CardF speciality={speciality} key={`speciality.id`} />
              </Col>
            ))}
        </Row>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    specialities: state.specialities,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    listSpecialities: () => dispatch(specialityActions.listSpecialities()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AVAILABLESPE);
