import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { Check, Upload, X } from 'react-feather';
//MY IMPORTS 
import { connect } from 'react-redux';
//Toaster Code
import { toast } from "react-toastify";
import {
  Button, Card, CardBody, CardTitle, Col, Container, Input, Label, Row
} from 'reactstrap';
import SuccessSound from "../../assets/success-sound-effect.mp3";





 


const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};



function LivePreviewExample(props) {


  //MY STATES

  const [email,setEmail] = useState(props.auth.user?.email)
  const [tel,setTel] = useState(props.auth.user?.tel)
  const [username,setUsername] = useState(props.auth.user?.username)
  const [lastname,setLastname] = useState(props.auth.user?.last_name)
  const [firstname,setFirstname] = useState(props.auth.user?.first_name)
  //FINISH MY STATES

//////////////////////////////////////////////

  //MY FUNCTIONS
  const handleEmail = (e) => {
    setEmail(e.target.value)
    
  }
  const handleTel = (e) => {
    setTel(e.target.value)
  }
  const handleUsername = (e) => {
    setUsername(e.target.value)
  }
  const handleLastName = (e) => {
    setLastname(e.target.value)
  }
  const handleFirstName = (e) => {
    setFirstname(e.target.value)
  }
  const handleSubmit = (e) => {
    axios
        .put(
          "http://localhost:8000/auth-api/update_patient_profile/" +
            props.auth.user?.id,
          {
            email:email,
            tel: tel,
            username: username,
            first_name: firstname,
            last_name: lastname,
            profile_img_url:props.auth.user?.profile_img_url
          }
        )
        .then((response) => {
          const sound = new Audio(SuccessSound);
          sound.volume = 0.007;
          sound.play();
          successNotify("success", "Success Updating Profile Info");
        })
        .catch((err) => {
          successNotify("fail", "Fail Updating Profile Info");
        });
  }
  //END MY FUNCTIONS


  const [files, setFiles] = useState([]);//CHANGE : implement the updating image profile functionality
  const {
    isDragActive,
    isDragAccept,
    isDragReject,
    open,
    getRootProps,
    getInputProps
  } = useDropzone({
    noClick: true,
    noKeyboard: true,
    multiple: false,
    accept: 'image/*',
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
    }
  });

  const thumbs = files.map((file) => (
    <div
      key={file.name}
      className="rounded-circle avatar-image overflow-hidden d-140 bg-neutral-success text-center font-weight-bold text-success d-flex justify-content-center align-items-center"
      
      >
      <img
        className="img-fluid img-fit-container rounded-sm"
        src={file.preview}
        alt="..."
      />
    </div>
  ));

  useEffect(
    () =>{
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
    
  );

  return (
    <>
      <div className="app-inner-content-layout">
        <div className="app-inner-content-layout--main  p-0">
          <div className="hero-wrapper mx-5 rounded-bottom shadow-xxl bg-composed-wrapper bg-second">
            <div className="flex-grow-1 w-100 d-flex align-items-center">
              <div
                className="bg-composed-wrapper--image opacity-3"
              />
              <div className="bg-composed-wrapper--bg bg-deep-sky opacity-5" />
              <div className="bg-composed-wrapper--content px-3 pt-5">
                <Container className="pt-4 pb-4">
                <div className="d-block d-md-flex align-items-start">
                    <div className="dropzone rounded-circle  mr-md-3">
                      <div
                        {...getRootProps({
                          className: 'dropzone-upload-wrapper'
                        })}>
                        <input {...getInputProps()} />
                        <div className="dropzone-inner-wrapper d-140 rounded-circle dropzone-avatar">
                          <div className="avatar-icon-wrapper d-140 rounded-circle m-2">
                            <Button
                              color="link"
                              onClick={open}
                              className="avatar-button badge shadow-sm-dark btn-icon badge-position badge-position--bottom-right border-2 text-indent-0 d-40 badge-circle badge-first text-white">
                              <Upload className="d-20" />
                            </Button>

                            <div>
                              {isDragAccept && (
                                <div className="rounded-circle overflow-hidden d-140 bg-success text-center font-weight-bold text-white d-flex justify-content-center align-items-center">
                                  <Check className="d-40" />
                                </div>
                              )}
                              {isDragReject && (
                                <div className="rounded-circle overflow-hidden d-140 bg-danger text-center font-weight-bold text-white d-flex justify-content-center align-items-center">
                                  <X className="d-60" />
                                </div>
                              )}
                              {!isDragActive && (
                                <div className="rounded-circle overflow-hidden d-140 bg-second text-center font-weight-bold text-white-50 d-flex justify-content-center align-items-center">
                                  <img
                                    className="img-fluid img-fit-container rounded-sm"
                                    src={props.auth.user?.profile_img_url}
                                    alt="..."
                                  />
                                </div>
                              )}
                            </div>

                            {thumbs.length > 0 && <div>{thumbs}</div>}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex text-white flex-column pl-md-2">
                      <div className="d-block d-md-flex align-items-center">
                        <div className="my-3 my-md-0">
                          <div className="d-flex align-items-end">
                            <div className="font-size-xxl font-weight-bold">
                              {props.auth.user?.username}
                            </div>
                          </div>
                          
                        </div>
                        
                      </div>
                      
                      
                    </div>
                  </div>
                </Container>
              </div>
            </div>
          </div>
          
          {/* USER FORM MODIFY PERSONAL INFO */}
          <>
          <Container className="mt-4">
      <Row md="12" className="justify-content-md-center">
        <Col md="6">
          <Card className="card-box mb-5">
            <CardBody>
              <CardTitle className="font-weight-bold font-size-lg mb-4">
                Personal Information
              </CardTitle>
              
                  <Label  className="mb-1"> Email</Label>
                  <Input
                    type="email"
                    placeholder=""
                    value={email}
                    onChange={handleEmail}
                    className="mb-4"
                  />
                  <Label  className="mb-1"> Telephone Number</Label>
                  <Input
                    type="Telephone"
                    placeholder=""
                    value={tel}
                    onChange={handleTel}
                    className="mb-4"
                  />
                  <Label  className="mb-1"> Last Name</Label>
                  <Input
                    type="text"
                    placeholder=""
                    value={lastname}
                    onChange={handleLastName}
                    className="mb-4"
                  />
                   <Label  className="mb-1"> First Name</Label>
                  <Input
                    type="text"
                    placeholder=""
                    value={firstname}
                    onChange={handleFirstName}
                    className="mb-4"
                  />
                  <Label  className="mb-1"> Username</Label>
                  <Input
                    type="text"
                    placeholder=""
                    value={username}
                    onChange={handleUsername}
                    className="mb-4"
                  />
                
                <Button color="primary" className="mt-1" type="submit" onClick={handleSubmit}>
                  Submit Changes
                </Button>
              
            </CardBody>
          </Card>
        </Col>
      </Row>
      </Container>
    </>
          
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(LivePreviewExample);