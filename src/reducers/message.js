import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../config/utility";

const initialState = {
  messages: [],
  chats: [],
  loading: true,
};

const addMessage = (state, action) => {
  return updateObject(state, {
    messages: [...state.messages, action.message],
  });
};

const setMessages = (state, action) => {
  return updateObject(state, {
    messages: action.messages.reverse(),
    loading: false,
  });
};

const setLoading = (state, action) => {
  return updateObject(state, {
    loading: action.loading,
  });
};
const setChats = (state, action) => {
  return updateObject(state, {
    chats: action.chats,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_MESSAGE:
      return addMessage(state, action);
    case actionTypes.LOADING_MESSAGES:
      return setLoading(state, action);
    case actionTypes.SET_MESSAGES:
      return setMessages(state, action);
    case actionTypes.GET_CHATS_SUCCESS:
      return setChats(state, action);
    default:
      return state;
  }
};

export default reducer;
