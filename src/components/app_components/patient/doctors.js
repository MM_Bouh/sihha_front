import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import * as messageActions from "../../../actions/message";
import WebSocketInstance from "../../../websocket";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Table,
  CardBody,
  Card,
  Badge,
  UncontrolledTooltip,
  Button,
  Progress,
} from "reactstrap";

import { Box, MessageCircle } from "react-feather";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
//import ChatHome from './ChatHome'

function LivePreviewExample(props) {
  //const isInitialRender = useRef(false)

  useEffect(() => {
    props.getUserChats(props.token);
  }, []);

  //STATES
  //   const [patients, setPatients] = useState([]);
  //const [doctors, setDoctors] = useState([]);

  return (
    <>
      <div>
        <Card className="card-box mt-12">
          <div className="card-header pr-2">
            <div className="card-header--title">Doctors</div>
          </div>
          <CardBody>
            <div className="table-responsive-md">
              <Table hover borderless className="text-nowrap mb-0">
                <thead>
                  <tr>
                    <th className="text-left">Doctor</th>
                    <th className="text-center">Consultation Status</th>
                    <th className="text-center">Consultation</th>
                    {/* <th className="text-center">Medical Record</th> */}
                  </tr>
                </thead>
                <tbody>
                  {props.chats.map((chat, index) => (
                    <tr key={index}>
                      <td>
                        <div className="d-flex align-items-center">
                          <div className="avatar-icon-wrapper mr-3">
                            <div className="avatar-icon">
                              <img alt="..." src={chat.doctor_image} />
                            </div>
                          </div>
                          <div>
                            <a
                              href="#/"
                              onClick={(e) => e.preventDefault()}
                              className="font-weight-bold text-black"
                              title="..."
                            >
                              {chat.doctor_name}
                            </a>
                          </div>
                        </div>
                      </td>

                      <td className="text-center">
                        <Badge
                          color={
                            chat.state === "PENDING"
                              ? "warning"
                              : chat.state === "ACTIVE"
                              ? "success"
                              : "first"
                          }
                          className="h-auto py-0 px-3"
                        >
                          {chat.state}
                        </Badge>
                      </td>
                      {/* The patient can only enter a conversation if it is active or completed
                      This is why we are checking here
                      */}
                      {chat.state !== "PENDING" && (
                        <td className="text-center">
                          <Button
                            size="sm"
                            color="neutral-dark"
                            className="hover-scale-sm d-40 p-0 btn-icon"
                          >
                            <span className="sidebar-icon">
                              <Link
                                to={{
                                  pathname: `/chat/${chat.id}`,
                                  state: {
                                    // doctor_last_name: chat.doctor_last_name,
                                    doctor_username: chat.doctor_name,
                                  },
                                }}
                              >
                                <MessageCircle />
                              </Link>
                            </span>
                          </Button>
                        </td>
                      )}
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </CardBody>
        </Card>

        {/* <ChatHome />        */}
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  token: state.auth.access,
  // username: state.auth.user.username,
  chats: state.message.chats,
});
const mapDispatchToProps = (dispatch) => {
  return {
    getUserChats: (token) => dispatch(messageActions.getUserChats(token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LivePreviewExample);
