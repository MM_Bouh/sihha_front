import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Badge,
  UncontrolledTooltip,
  Button,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";
import { connect } from "react-redux";
import avatar2 from "../../assets/images/avatars/avatar2.jpg";
import { NavLink } from "react-router-dom";
import auth from "reducers/auth";

const SidebarUserbox = (props) => {
  return (
    <>
      <div className="app-sidebar--userbox">
        <UncontrolledDropdown className="card-tr-actions"></UncontrolledDropdown>
        <div className="avatar-icon-wrapper avatar-icon-md">
          {/* <Badge color="danger" className="badge-circle">
            Offline
          </Badge> */}
          <Badge color="success" className="badge-circle">
            Online
          </Badge>
          <div className="avatar-icon rounded-circle">
            {/* <img alt="..." src={props.authenticated && props.auth.user.profile_img_url} /> */}
            <img alt="..." src={props.auth.user?.profile_img_url} />
          </div>
        </div>
        <div className="my-3 userbox-details">
          {props.authenticated && props.auth.user.last_name}
        </div>
        {/* <Button size="sm" tag={NavLink} to="/PageProfile" color="userbox">
          View profile
        </Button> */}
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({
  auth: state.auth,
  authenticated: state.auth.isAuthenticated,
});
export default connect(mapStateToProps)(SidebarUserbox);
