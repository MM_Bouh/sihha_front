import axios from "axios";
import {
  AUTH_ERROR,
  CLEAN_SESSION,
  HOST,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT,
  USER_LOADED,
  USER_LOADING,
} from "./type";
export const login = (tel, password) => (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ tel, password });
  axios
    .post(HOST + "auth-api/login/", body, config)
    .then((res) => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: LOGIN_FAIL,
        payload: err.response.data,
      });
      console.log("This is the error", err.response.data);
    });
};

export const loadUser = () => (dispatch, getState) => {
  dispatch({ type: USER_LOADING });
  const access = getState().auth.access;

  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  let url = "";
  if (access) {
    config.headers["Authorization"] = `JWT ${access}`;
    const tokenParts = JSON.parse(atob(access.split(".")[1]));

    url = "auth-api/get_current_user/";

    //console.log(tokenParts);
    axios
      .get(HOST + url, config)
      .then((res) => {
        dispatch({
          type: USER_LOADED,
          payload: res.data,
        });
      })
      .catch((err) => {
        dispatch({
          type: AUTH_ERROR,
        });
        console.log(err.response.data);
      });
  } else {
    dispatch({
      type: AUTH_ERROR,
    });
  }
};
export const logout = () => (dispatch, getState) => {
  const refresh_token = getState().auth.refresh;
  const body = { refresh_token };
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  axios.post(HOST + "auth-api/logout/", body, config).then(() => {
    dispatch({
      type: CLEAN_SESSION,
    });
    dispatch({
      type: LOGOUT,
    });
  });
  // .catch((err) => console.log(err.response.data));
};
