import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import { FaCommentMedical } from "react-icons/fa";
import { connect } from "react-redux";
import { Button, Card, Col, Row } from "reactstrap";
import { setHeaderSearchHover } from "../../../reducers/ThemeOptions";

const HeaderSearch = (props) => {
  const { headerSearchHover, setHeaderSearchHover } = props;
  const [doctors, setDoctors] = useState([]);
  const [loading, setLoading] = useState(true);

  const [filteredData, setFilteredData] = useState([]);
  const [wordEntered, setWordEntered] = useState("");

  const cardStyle = {
    transition: "all 0.3s ease-in",
  };
  const writeStyle = {
    color: "#5A20CB",
  };
  const titleStyle = {
    color: "#1B98F5",
  };

  const toggleHeaderSearchHover = () => {
    setHeaderSearchHover(!headerSearchHover);
  };

  useEffect(() => {
    axios.get("http://localhost:8000/auth-api/doctors/").then((res) => {
      setDoctors(res.data);
      console.log("These are the doctors from the searchBar: ", res.data);
      setLoading(false);
    });
    //console.log(filteredData);
    // dispatch(listDoctors(keyword))
    //props.listDoctors()
  }, []);

  const handleFilter = (event) => {
    const searchWord = event.target.value;
    setWordEntered(searchWord);
    const newFilter = doctors.filter((value) => {
      return (
        value.username.toLowerCase().includes(searchWord.toLowerCase()) ||
        value.specialityName.toLowerCase().includes(searchWord.toLowerCase())
      );
    });

    if (searchWord === "") {
      setFilteredData([]);
    } else {
      setFilteredData(newFilter);
      //console.log("This is the filtred data", filteredData);
    }
  };

  return (
    <>
      <React.Fragment>
        <div className="header-search-wrapper">
          <div
            className={clsx("search-wrapper", {
              "is-active": headerSearchHover,
            })}
          >
            <label
              className="icon-wrapper text-black"
              htmlFor="header-search-input"
            >
              <FontAwesomeIcon icon={["fas", "search"]} />
            </label>
            <input
              onFocus={toggleHeaderSearchHover}
              onBlur={toggleHeaderSearchHover}
              className="form-control"
              id="header-search-input"
              name="header-search-input"
              placeholder="Search by a Doctor's name or speciality 1.."
              type="search"
              //data={doctors}
              value={wordEntered}
              onChange={handleFilter}
            />
          </div>
        </div>

        <div>
          {filteredData.length != 0 && (
            <div>
              <div
                className="font-size-xl font-weight-bold pt-1 pb-3 text-center"
                style={titleStyle}
              >
                AVAILABLE DOCTORS
              </div>
              <Row>
                {filteredData.slice(0, 15).map((doctor) => (
                  <Col md="4">
                    <Card
                      className="card-box mb-5 hover-scale-rounded"
                      style={cardStyle}
                    >
                      <div className="text-center py-3">
                        <div className="d-90 rounded-circle border-0 my-2 card-icon-wrapper bg-plum-plate btn-icon mx-auto text-center">
                          <div className="avatar-icon-wrapper d-80">
                            <div className="avatar-icon d-80 rounded-circle">
                              <img alt="..." src={doctor.profile_img_url} />
                            </div>
                          </div>
                        </div>
                        <div
                          className="font-size-xl font-weight-bold pt-2"
                          style={writeStyle}
                        >
                          Dr. {doctor.username}
                        </div>
                        <br />
                        <span className="opacity-5 pb-3">Speciality: </span>
                        <span
                          className="font-size-md font-weight-bold pt-2 text-black"
                          style={writeStyle}
                        >
                          {doctor.specialityName}
                        </span>

                        <div className="divider mx-auto w-50 my-3" />
                        <span className="opacity-5 pb-3">Experience: </span>
                        <span
                          className="font-size-md font-weight-bold pt-2 text-black"
                          style={writeStyle}
                        >
                          {doctor.experience}{" "}
                          {doctor.experience > 1 ? "years" : "year"}
                        </span>
                        <div className="divider mx-auto w-50 my-3" />
                        <div className="text-center">
                          <Button
                            className="p-0 d-50 btn-icon btn-pill mx-1 btn-transition-none"
                            color="first"
                            //style={writeStyle}
                            outline
                          >
                            <span className="btn-wrapper--icon">
                              {/* <FontAwesomeIcon icon={['far', 'heart']} /> */}
                              <FaCommentMedical />
                            </span>
                          </Button>
                        </div>
                      </div>
                    </Card>
                  </Col>
                ))}
              </Row>
            </div>
          )}
        </div>
      </React.Fragment>
    </>
  );
};

const mapStateToProps = (state) => ({
  headerSearchHover: state.ThemeOptions.headerSearchHover,
});

const mapDispatchToProps = (dispatch) => ({
  setHeaderSearchHover: (enable) => dispatch(setHeaderSearchHover(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderSearch);
