export const HOST = "http://localhost:8000/";
export const host = "http://localhost:8000";

export const CLEAN_SESSION = "CLEAN_SESSION";

export const USER_LOADING = "USER_LOADING";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const LOGOUT = "LOGOUT";
export const CREATE_MESSAGE = "CREATE_MESSAGE";
export const ERROR_CREATE_MESSAGE = "ERROR_CREATE_MESSAGE";
export const GET_MESSAGES = "GET_MESSAGES";
export const ERROR_GET_MESSAGES = "ERROR_GET_MESSAGES";
