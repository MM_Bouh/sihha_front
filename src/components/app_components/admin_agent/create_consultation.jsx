import axios from "axios";
import React, { useEffect, useState } from "react";
import { Typeahead } from 'react-bootstrap-typeahead';
//Toaster Code 
import { toast } from "react-toastify";
import {
  Button, Container, Modal
} from "reactstrap";
import SuccessSound from "../../assets/success-sound-effect.mp3";



const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};
const ModalConsultation = (props) => {
    //My states
    const [patients,setPatients] = useState([])
    const [doctors,setDoctors] = useState([])
    const [patientSelected,setPatientSelected] = useState("")
    const [doctorSelected,setDoctorSelected] = useState("")
    //UseEffect
    useEffect(()=>{
      //Get the doctors and patients list
      axios
          // .get('https://medicare-back.herokuapp.com/auth-api/patients/')
          .get('http://localhost:8000/auth-api/patients/')
          .then((res) => {
            setPatients(res.data)
          })
          .catch((err) => {
  
          })
      axios
      // .get('https://medicare-back.herokuapp.com/auth-api/doctors/')
      .get('http://localhost:8000/auth-api/doctors/')
      .then((res) => {
        console.log(res.data)
        res.data.map((doctor) => doctor.username= 'dr. ' + doctor.username)
        setDoctors(res.data)
      })
      .catch((err) => {
  
      })
    },[])
  
  
  
  
  
    //My Functions 
  
  
    /////////
    //Values For creations
    const handleSeletedDoctor = (e) => {
      
      {e && e[0] && setDoctorSelected(e[0].id)}
    }
  
    const handleSeletedPatient = (e) => {
      {e && e[0] && setPatientSelected(e[0].id)}
    }
    /////////
  
  
  
    const cancelCreateConsultation = () => {
      //return to the root page for the admin agent
      window.location= "/AdminConsultations"
      
    }
    const createConsultation = () => {
    //config
    const config = {
      headers: {
      "Content-Type": "application/json",
      },
    };
    //body
    const body = {
        
        doctor:doctorSelected,
        patient:patientSelected,
        admin_agent:props.user
  
    };
      axios
          // .post('https://medicare-back.herokuapp.com/chat-api/create_conversation/',body,config)
          .post('http://localhost:8000/chat-api/create_conversation/',body,config)
          .then((res) => {
              if(res.data['error']){
                const sound = new Audio(SuccessSound);
                sound.volume = 0.008;
                sound.play();
                successNotify('fail','Sorry, you already have a conversation going on with this doctor!')
              }else{
                const sound = new Audio(SuccessSound);
                sound.volume = 0.007;
                sound.play();
                successNotify('success','Consultation Created')
                setTimeout(()=>{
    
                },[3000]);
                window.location= "/AdminConsultations"
              }
              
          })
      
    }
    return (
      <>
        <div className="d-flex align-items-center justify-content-center flex-wrap">
  
  
         
          <Modal
            zIndex={2000}
            centered
            size="md"
            isOpen={true}
            toggle={true}
            contentClassName="border-0">
           <div className="text-center p-5">
              
           
           <Container className='mb-3'>
           <span className="font-weight-bold text-black">Patient</span>
            <Typeahead
              id="typeID2"
              labelKey="username"
              multiple={false}
              options={patients && patients}
              placeholder="Choose a patient..."
              onChange={handleSeletedPatient}
            />
          </Container>
            
          <Container>
          <span className="font-weight-bold text-black">Doctor</span>
            <Typeahead
              id="typeID2"
              labelKey="username"
              multiple={false}
              options={doctors && doctors}
              placeholder="Choose a doctor..."
              onChange={handleSeletedDoctor}
            />
          </Container>
              <div className="pt-4">
                <Button
                  onClick={cancelCreateConsultation}
                  color="neutral-dark"
                  className="btn-pill mx-1">
                  <span className="btn-wrapper--label">Cancel</span>
                </Button>
                <Button
                  onClick={createConsultation}
                  color="success"
                  className="btn-pill mx-1">
                  <span className="btn-wrapper--label">Create</span>
                </Button>
              </div>
            </div>
          </Modal>
        </div>
      </>
    );
  }

  export default ModalConsultation;