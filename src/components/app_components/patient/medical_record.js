import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import clsx from "clsx";
import React, { Component, useEffect, useRef, useState } from "react";
import { useDropzone } from "react-dropzone";
import { Check, UploadCloud, X } from "react-feather";
import { FaAddressBook, FaFileMedical, FaFileMedicalAlt } from "react-icons/fa";
import { RiQuestionAnswerLine } from "react-icons/ri";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import {
  Alert,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Collapse,
  Container,
  Form,
  Input,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import SuccessSound from "../../assets/success-sound-effect.mp3";
import UpdateQuestions from "./update_questions";

const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};

function usePrevious(value) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

const MedicalRecordFiles = (props) => {
  const previousLoading = usePrevious(props.loading);
  useEffect(() => {
    if (props.loading === false && previousLoading) {
      setFiles(props.files);
    }
  });
  const [files, setFiles] = useState([]);
  const {
    isDragActive,
    isDragAccept,
    isDragReject,
    getRootProps,
    getInputProps,
  } = useDropzone({
    accept: "image/jpeg, image/png,.pdf,.txt,.docx,.xlsx",
  });

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////   FUNCTIONS  ///////////////////////////////////////////////////////////

  //THE FUNCTION THAT DEAL WITH PUTTING THE NEW FILES WITH THE EXISTING FILES
  const handleUploadFiles = (e) => {
    // Here i get the files from the input
    let uploadedFiles = e.target.files;

    //Here i set the new files in the files state
    setFiles([...files, ...uploadedFiles]);
  };

  //FUNCTION OF DELETING A FILE (IT DEPENDS  IF THE FILE IS ALREADY IN THE SERVER OR IT IS NOT)
  const handleDeleteFile = (file, i) => {
    //file.id means if there is an id it means it is already in the server the reason for that is the response from the server contains an id
    if (file.id) {
      //Delete the files from the server
      axios
        // .delete("https://medicare-back.herokuapp.com/medical-record-api/medical_files/"+file.id)
        .delete(
          "http://localhost:8000/medical-record-api/medical_files/" + file.id
        )
        .then((res) => {
          const newFilesList = files.slice();
          newFilesList.splice(i, 1);

          setFiles(newFilesList);
          successNotify("success", "success deleting the file");
        })
        .catch((err) => {
          successNotify("fail", "fail deleting the file");
        });
    } else {
      //Delete the files from the local state
      const newFilesList = files.slice();
      newFilesList.splice(i, 1);

      setFiles(newFilesList);
      successNotify("success", "success deleting the file");
    }
  };

  //PROGRESS FUNCTION
  // const options = {
  //   onUploadProgress: (progressEvent) => {
  //     const {loaded, total} = progressEvent;
  //     let percent = Math.floor( (loaded * 100) / total )
  //     console.log( `${loaded}kb of ${total}kb | ${percent}%` );  // THIS IS A FUNCTION THAT CAN BE USED IN AXIOS TO SHOW THE PROGRESS OF UPLOADING

  //     if( percent < 100 ){
  //       this.setState({ uploadPercentage: percent })
  //     }
  //   }
  // }

  //FUNCTION FOR UPLOADING THE FILES TO THE SERVER
  const handleSubmit = () => {
    let newArray = files.slice();
    //Upload files one by one
    newArray.map((file, i) => {
      //Check if the file is not already in the server
      if (!file.id) {
        //Create a FormData
        let formData = new FormData();
        formData.append("medical_file", file);
        formData.append("medical_record", props.medical_record_id);

        //Array that will update the files uploaded state

        //API URL
        console.log(props.medical_record_id);
        // let url = 'https://medicare-back.herokuapp.com/medical-record-api/medical_files/'+props.medical_record_id
        let url =
          "http://localhost:8000/medical-record-api/medical_files/" +
          props.medical_record_id;
        axios
          .post(url, formData, {
            headers: {
              "content-type": "multipart/form-data",
            },
          })
          .then((res) => {
            successNotify("success", "success uploading the file " + file.name);

            // Update the files state with the result of adding the file to server
            newArray[i] = res.data;
            setFiles([...newArray]);
          })
          .catch((err) => {
            successNotify(
              "fail",
              "failed uploading the file " + file.medical_file
            );
          });
      }
    });
  };
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //HERE IS THE CONSTANT THAT RETURNS THE HTML FOR LISTING THE FILES
  const listFiles = files.map((file, i) => (
    <Row xs={1} md={1} lg={1} key={i}>
      <ListGroupItem
        className="font-size-sm px-3 py-2 text-primary d-flex justify-content-between align-items-center"
        key={i}
      >
        {/* Here Medical File name*/}
        {/* Here i am going to check if it is a file comming from the server or an not || Because the name of the 2 types is not the same*/}

        <Col xs={6} md={6} lg={6}>
          {file.id ? (
            <span>{file.medical_file}</span>
          ) : (
            <span>{file.name}</span>
          )}{" "}
        </Col>

        {/* Here It indicates if it is uploaded or not (It depends if it's already in the server or not) */}
        <Col xs={4} md={4} lg={4}>
          {!file.id ? (
            <span className="badge badge-pill bg-neutral-warning text-warning">
              Not upload yet
            </span>
          ) : (
            <span className="badge badge-pill bg-neutral-success text-success">
              Uploaded
            </span>
          )}
        </Col>

        {/* Here Medical the button for deleting a medical file*/}
        <Col xs={2} md={2} lg={2}>
          <Button
            color="neutral-danger"
            size="sm"
            onClick={(e) => {
              e.preventDefault();
              handleDeleteFile(file, i); //Send the file and the index
            }}
          >
            <span className="btn-wrapper--icon">
              <FontAwesomeIcon icon={["far", "trash-alt"]} />
            </span>
          </Button>
        </Col>
      </ListGroupItem>
    </Row>
  ));

  //RETURN
  return (
    <>
      <div className="dropzone">
        <div {...getRootProps({ className: "dropzone-upload-wrapper" })}>
          {/*HERE I DIDNT USE THE LIBRARY DROPZONE ONCHANGE DEFAULT FUNCTION BECAUSE I HAD PROBLEMS SETTING INITIAL VALUES COMMING FROM THE SERVER*/}
          {/*SO HANDLEUPLOADFILES IS A FUNCTION  I USE INSTEAD IF THE DROPZONE DEFAULT FUNCTION AND I LET DROPZPONE CODE BECAUSE OF THE NICE LAYOUT*/}
          <input {...getInputProps()} onChange={handleUploadFiles} />
          <div className="dropzone-inner-wrapper">
            {isDragAccept && (
              <div>
                <div className="d-100 btn-icon mb-3 hover-scale-lg bg-success shadow-success-sm rounded-circle text-white">
                  <Check className="d-50" />
                </div>
                <div className="font-size-sm text-success">
                  All files will be uploaded!
                </div>
              </div>
            )}
            {isDragReject && (
              <div>
                <div className="d-100 btn-icon mb-3 hover-scale-lg bg-danger shadow-danger-sm rounded-circle text-white">
                  <X className="d-50" />
                </div>
                <div className="font-size-sm text-danger">
                  Some files will be rejected!
                </div>
              </div>
            )}
            {!isDragActive && (
              <div>
                <div className="d-100 btn-icon mb-3 hover-scale-lg bg-white shadow-light-sm rounded-circle text-primary">
                  <UploadCloud className="d-50" />
                </div>
                <div className="font-size-sm">
                  Drag and drop Medical Files here{" "}
                  <span className="font-size-xs text-dark">(pdf,jpg,png)</span>
                </div>
              </div>
            )}

            <small className="py-2 text-black-50">or</small>
            <div>
              <Button
                color="primary"
                className="hover-scale-sm font-weight-bold btn-pill px-4"
              >
                <span className="px-2">Browse Files</span>
              </Button>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="font-weight-bold my-4 text-uppercase text-dark font-size-sm text-center">
          Uploaded Files
        </div>
        {listFiles.length <= 0 && (
          <div className="text-info text-center font-size-sm"></div>
        )}
        {listFiles.length > 0 && (
          <div>
            <Alert color="success" className="text-center mb-3">
              You have uploaded{" "}
              {listFiles.length === 1 ? (
                <b>{listFiles.length} file!</b>
              ) : (
                <b>{listFiles.length} files!</b>
              )}
            </Alert>

            {/*HERE I WILL CREATE A CONTAINER TO ORGANIZE THE LISTING OF FILES */}
            <Container fluid>
              <ListGroup className="font-size-sm">{listFiles}</ListGroup>
            </Container>
          </div>
        )}
        <div className="mt-3">
          <Button
            size="lg"
            color="primary"
            className="hover-scale-sm"
            onClick={handleSubmit}
          >
            Submit Files
          </Button>
        </div>
      </div>
    </>
  );
};

class LivePreviewExample extends Component {
  constructor(props) {
    super(props);
    this.toggleAccordion = this.toggleAccordion.bind(this);
    this.state = {
      accordion: [true, false, false],
      medical_record_id: "",
      name: props.auth.user.first_name,
      surname: props.auth.user.last_name,
      gender: "male",
      height: null,
      weight: null,
      bloodType: "",
      age: null,
      // questions: undefined,
      // answers: undefined,
      questions: [],
      answers: [],
      files: [],
      loading: undefined,
      modal1: false,
      modal2: false,
      modal3: false,
      modal4: false,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.toggleModal2 = this.toggleModal2.bind(this);
    this.toggleModal3 = this.toggleModal3.bind(this);
    this.toggleModal4 = this.toggleModal4.bind(this);
  }
  toggleModal() {
    this.setState({ ...this.state, modal1: !this.state.modal1 });
  }
  toggleModal2() {
    this.setState({ ...this.state, modal2: !this.state.modal2 });
  }
  toggleModal3() {
    this.setState({ ...this.state, modal3: !this.state.modal3 });
  }
  toggleModal4() {
    this.setState({ ...this.state, modal4: !this.state.modal4 });
  }
  componentDidMount() {
    //To get the basic questions
    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_basic_info/" +
        "http://localhost:8000/medical-record-api/update_medical_record_basic_info/" +
          this.props.auth.user.id
      )
      .then((res) => {
        this.setState({
          ...this.state,
          medical_record_id: res.data.id,
          age: res.data.age,
          bloodType: res.data.blood_type,
          height: res.data.height,
          weight: res.data.weight,
          gender: res.data.gender,
        });
      })
      .catch((err) => {
        console.log("Say Hello");
      });

    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/get_questions_questionnaire/"
        "http://localhost:8000/medical-record-api/get_questions_questionnaire/"
      )
      .then((res) => {
        this.setState({ questions: res.data });
      })
      .catch((err) => {
        console.log("Hello Question does not exit");
      });

    axios
      .get(
        // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_questionnaire/" +
        "http://localhost:8000/medical-record-api/update_medical_record_questionnaire/" +
          this.props.auth.user.id
      )
      .then((res) => {
        this.setState({ ...this.state, answers: res.data });
      })
      .catch((err) => {});

    this.setState({ ...this.state, loading: true });
  }

  toggleAccordion(tab) {
    const prevState = this.state.accordion;
    const state = prevState.map((x, index) => (tab === index ? !x : false));
    if (state[2] == true) {
      axios
        .get(
          // "https://medicare-back.herokuapp.com/medical-record-api/medical_files/" + //Change this because this charges the medical files for each toggle
          "http://localhost:8000/medical-record-api/medical_files/" + //Change this because this charges the medical files for each toggle
            this.state.medical_record_id
        )
        .then((res) => {
          this.setState({ ...this.state, files: res.data, loading: false });
          console.log("The list of the files : ", res.data);
        })
        .catch((err) => {
          this.setState({ ...this.state, loading: false });
        });
    }
    this.setState({
      accordion: state,
    });
  }

  render() {
    const handleAge = (e) => {
      this.setState({ age: e.target.value });
    };
    const handleBloodType = (e) => {
      this.setState({ bloodType: e.target.value });
    };
    const handleHeight = (e) => {
      this.setState({ height: e.target.value });
    };
    const handleWeight = (e) => {
      this.setState({ weight: e.target.value });
    };
    const handleGender = (e) => {
      this.setState({ gender: e.target.value });
    };

    //Submit Personal Information
    // const onFinishPersonalInfo = () => {
    //   //config
    //   const config = {
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //   };
    //   //body
    //   const body = {
    //     age: this.state.age,
    //     height: this.state.height,
    //     blood_type: this.state.bloodType,
    //     weight: this.state.weight,
    //     gender: this.state.gender,
    //     patient: this.props.auth.user.id,
    //   };
    //   axios
    //     .post(
    //       // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_basic_info/" +
    //       "http://localhost:8000/medical-record-api/answer_medical_record_basic_info/",
    //       body,
    //       config
    //     )
    //     .then((response) => {
    //       const sound = new Audio(SuccessSound);
    //       sound.volume = 0.007;
    //       sound.play();
    //       successNotify("success", "Success Submitting Personal Info");
    //     })
    //     .catch((err) => {
    //       successNotify("fail", "Failed Submitting Personal Info");
    //     });
    //   if (successNotify) {
    //     this.toggleModal3();
    //     this.state.age = body.age;
    //     this.state.height = body.height;
    //     this.state.bloodType = body.blood_type;
    //     this.state.weight = body.weight;
    //     this.state.gender = body.weight;
    //     this.props.history.push("/MedicalRecord");
    //   }
    // };

    //To update the personal info
    const onUpdatePersonalInfo = () => {
      axios
        .put(
          // "https://medicare-back.herokuapp.com/medical-record-api/update_medical_record_basic_info/" +
          "http://localhost:8000/medical-record-api/update_medical_record_basic_info/" +
            this.props.auth.user.id,
          {
            age: this.state.age,
            height: this.state.height,
            blood_type: this.state.bloodType,
            weight: this.state.weight,
            gender: this.state.gender,
          }
        )
        .then((response) => {
          const sound = new Audio(SuccessSound);
          sound.volume = 0.007;
          sound.play();
          successNotify("success", "Success Submitting Personal Info");
        })
        .catch((err) => {
          successNotify("fail", "Failed Submitting Personal Info");
        });
      if (successNotify) {
        this.toggleModal4();
      }
    };

    return (
      <>
        <div className="accordion mb-5">
          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[0],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(0)}
                      aria-expanded={this.state.accordion[0]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        {/* <IconContext.Provider value={{ color: "blue", className: "global-class-name" }}> */}
                        <FaAddressBook />
                        {/* </IconContext.Provider> */}
                        <span className="pl-4">Personal Information</span>
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>
              <Collapse isOpen={this.state.accordion[0]}>
                <div className="p-4">
                  {/* To answer the questions for the first time */}
                  <div className="form-row">
                    <div>
                      <Button
                        size="lg"
                        color="primary"
                        className="m-4"
                        onClick={this.toggleModal4}
                      >
                        <RiQuestionAnswerLine /> Open the Basic questions
                      </Button>
                    </div>

                    <div className="d-flex align-items-center justify-content-center flex-wrap">
                      {/* To answer the basic questions */}

                      {/* To update the questions  */}
                      <Modal
                        zIndex={2000}
                        centered
                        isOpen={this.state.modal4}
                        toggle={this.toggleModal4}
                        contentClassName="border-0"
                      >
                        <ModalHeader toggle={this.toggleModal4}>
                          The Basic Questions
                        </ModalHeader>
                        <ModalBody>
                          <Col md="12">
                            <Card className="card-box mb-5">
                              <CardBody>
                                <Form>
                                  <label htmlFor="name" className="mb-1">
                                    Name
                                  </label>
                                  <Input
                                    className="mb-2"
                                    placeholder="name"
                                    type="text"
                                    value={this.state.name}
                                    //onChange={handleName}
                                  />
                                  <label htmlFor="surname" className="mb-1">
                                    Surname
                                  </label>
                                  <Input
                                    className="mb-2"
                                    placeholder="surname"
                                    type="text"
                                    value={this.state.surname}
                                    //onChange={handleSurname}
                                  />
                                  <label htmlFor="age" className="mb-1">
                                    Age
                                  </label>

                                  <Input
                                    className="mb-2"
                                    placeholder="age"
                                    id="age"
                                    type="number"
                                    onChange={handleAge}
                                    value={this.state.age}
                                  />
                                  <div className="divider" />

                                  <label
                                    htmlFor="exampleCustomSelect"
                                    className="mb-1"
                                  >
                                    Gender
                                  </label>
                                  <Input
                                    className="mb-2"
                                    type="select"
                                    id="exampleCustomSelect"
                                    name="customSelect"
                                    onChange={handleGender}
                                    value={this.state.gender}
                                  >
                                    <option>male</option>
                                    <option>female</option>
                                  </Input>
                                  <label htmlFor="height" className="mb-1">
                                    Height
                                  </label>

                                  <Input
                                    className="mb-2"
                                    id="height"
                                    placeholder="height"
                                    type="number"
                                    onChange={handleHeight}
                                    value={this.state.height}
                                  />

                                  <label htmlFor="weight" className="mb-1">
                                    Weight
                                  </label>

                                  <Input
                                    className="mb-2"
                                    id="weight"
                                    placeholder="weight"
                                    type="number"
                                    onChange={handleWeight}
                                    value={this.state.weight}
                                  />
                                  <label htmlFor="blood-type" className="mb-1">
                                    Blood Type
                                  </label>
                                  <Input
                                    className="mb-2"
                                    type="select"
                                    id="exampleCustomSelect"
                                    name="customSelect"
                                    onChange={handleBloodType}
                                    value={this.state.bloodType}
                                  >
                                    <option></option>
                                    <option>A+</option>
                                    <option>A-</option>
                                    <option>B+</option>
                                    <option>B-</option>
                                    <option>AB+</option>
                                    <option>AB-</option>
                                    <option>O+</option>
                                    <option>O-</option>
                                  </Input>
                                </Form>
                              </CardBody>
                            </Card>

                            <div className="card-box mb-2">
                              <Button
                                type="submit"
                                color="primary"
                                // className="ml-auto hover-scale-sm font-weight-bold  px-4"
                                className="ml-auto font-weight-bold  px-4"
                                // className="ml-auto"
                                onClick={onUpdatePersonalInfo}
                                disabled={
                                  this.state.height === null ||
                                  this.state.weight === null ||
                                  this.state.bloodType === "" ||
                                  this.state.age === null
                                }
                              >
                                Submit Information
                              </Button>
                            </div>
                          </Col>
                        </ModalBody>
                      </Modal>
                    </div>
                  </div>
                </div>
              </Collapse>
            </Card>
          </Card>
          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[1],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(1)}
                      aria-expanded={this.state.accordion[1]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        {/* <FaFileMedicalAlt /> <span className="pl-0 pl-sm-0 pl-md-3 pl-lg-4 pl-xl-5" >Medical Questionnaire</span> */}
                        <FaFileMedicalAlt />{" "}
                        <span className="pl-4">Medical Questionnaire</span>
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>
              <Collapse isOpen={this.state.accordion[1]}>
                <div className="p-4">
                  <div className="form-row">
                    <div className="card-box mb-2">
                      <Button
                        size="lg"
                        color="primary"
                        className="m-4"
                        onClick={this.toggleModal}
                      >
                        <RiQuestionAnswerLine /> Open The Questionnaire
                      </Button>

                      {/* Modal To update the Questions */}
                      <div className="d-flex align-items-center justify-content-center flex-wrap">
                        <Modal
                          zIndex={2000}
                          centered
                          isOpen={this.state.modal1}
                          toggle={this.toggleModal}
                          contentClassName="border-0"
                        >
                          <ModalHeader toggle={this.toggleModal}>
                            The Questionnaire
                          </ModalHeader>
                          <ModalBody>
                            <UpdateQuestions />
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="link"
                              className="btn-link-dark"
                              onClick={this.toggleModal}
                            >
                              Close
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                    </div>
                  </div>
                </div>
              </Collapse>
            </Card>
          </Card>

          <Card
            className={clsx("card-box", {
              "panel-open": this.state.accordion[2],
            })}
          >
            <Card>
              <CardHeader>
                <div className="panel-title">
                  <div className="accordion-toggle">
                    <Button
                      color="link"
                      size="lg"
                      className="d-flex align-items-center justify-content-between"
                      onClick={() => this.toggleAccordion(2)}
                      aria-expanded={this.state.accordion[2]}
                    >
                      <h5 className="font-size-xl font-weight-bold">
                        {/* <FaFileMedical /> <span className="pl-0 pl-sm-0 pl-md-3 pl-lg-4 pl-xl-5" >Medical Files</span> */}
                        <FaFileMedical />{" "}
                        <span className="pl-4">Medical Files</span>
                      </h5>
                      <FontAwesomeIcon
                        icon={["fas", "angle-up"]}
                        className="font-size-xl accordion-icon"
                      />
                    </Button>
                  </div>
                </div>
              </CardHeader>
              <Collapse isOpen={this.state.accordion[2]}>
                <div className="p-4">
                  <MedicalRecordFiles
                    files={this.state.files}
                    loading={this.state.loading}
                    medical_record_id={this.state.medical_record_id}
                  />
                </div>
              </Collapse>
            </Card>
          </Card>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(LivePreviewExample);
