import axios from "axios";
import {
  SPECIALITY_LIST_REQUEST,
  SPECIALITY_LIST_SUCCESS,
  SPECIALITY_LIST_FAIL,
} from "./actionTypes";

export const listSpecialities = () => async (dispatch) => {
  try {
    dispatch({ type: SPECIALITY_LIST_REQUEST });

    const { data } = await axios.get(
      "http://localhost:8000/auth-api/specialities/"
    );

    dispatch({
      type: SPECIALITY_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SPECIALITY_LIST_FAIL,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};
