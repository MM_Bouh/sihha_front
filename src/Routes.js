///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//My imports
//1.AUTH
import Login from "components/app_components/auth/login";
import Register from "components/app_components/auth/register";
import PrivateRoute from "components/app_components/PrivateRoute";
import { AnimatePresence, motion } from "framer-motion";
import React, { lazy, Suspense } from "react";
import { Redirect, Route, Switch, useLocation } from "react-router-dom";
import { ClimbingBoxLoader } from "react-spinners";
// Layout Blueprints
import {
  CollapsedSidebar,
  LeftSidebar,
  MinimalLayout,
  PresentationLayout,
} from "./components/layout-blueprints";
import Hoc from "./hoc/hoc";

//2.PATIENT
const MedicalRecord = lazy(() =>
  import("components/app_components/patient/medical_record")
);
const Doctors = lazy(() => import("components/app_components/patient/doctors"));
const Home = lazy(() => import("components/app_components/patient/home"));

const Profile = lazy(() => import("components/app_components/patient/profile"));

//3.DOCTOR

const Patients = lazy(() =>
  import("components/app_components/doctor/patients")
);
const ProfileDoctor = lazy(() =>
  import("components/app_components/doctor/profile")
);
const MedicalRecord_patient = lazy(() =>
  import("components/app_components/doctor/medical_record_patient")
);

//4.ADMIN AGENT
const CreateConsultatoin = lazy(() =>
  import("components/app_components/admin_agent/create_consultation")
);
const CreateDoctor = lazy(() =>
  import("components/app_components/admin_agent/create_doctor")
);
const AdminConsultations = lazy(() =>
  import("components/app_components/admin_agent/admin_consultations")
);
const ProfileAgent = lazy(() =>
  import("components/app_components/admin_agent/profile")
);

// Chat
const Chat = lazy(() => import("./containers/Chat"));
const ChatTest = lazy(() => import("./containers/ChatTest"));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Unutil Imports

// const Overview = lazy(() => import("./example-pages/Overview"));
// const DashboardMonitoring = lazy(() =>
//   import("./example-pages/DashboardMonitoring")
// );
// const DashboardCommerce = lazy(() =>
//   import("./example-pages/DashboardCommerce")
// );
// const DashboardAnalytics = lazy(() =>
//   import("./example-pages/DashboardAnalytics")
// );
// const DashboardStatistics = lazy(() =>
//   import("./example-pages/DashboardStatistics")
// );
// const ElementsAvatars = lazy(() => import("./example-pages/ElementsAvatars"));
// const ElementsBadges = lazy(() => import("./example-pages/ElementsBadges"));
// const ElementsButtons = lazy(() => import("./example-pages/ElementsButtons"));
// const ElementsDropdowns = lazy(() =>
//   import("./example-pages/ElementsDropdowns")
// );
// const ElementsIcons = lazy(() => import("./example-pages/ElementsIcons"));
// const ElementsNavigationMenus = lazy(() =>
//   import("./example-pages/ElementsNavigationMenus")
// );
// const ElementsPagination = lazy(() =>
//   import("./example-pages/ElementsPagination")
// );
// const ElementsProgressBars = lazy(() =>
//   import("./example-pages/ElementsProgressBars")
// );
// const ElementsRatings = lazy(() => import("./example-pages/ElementsRatings"));
// const ElementsRibbons = lazy(() => import("./example-pages/ElementsRibbons"));
// const ElementsScrollable = lazy(() =>
//   import("./example-pages/ElementsScrollable")
// );
// const ElementsSearchBars = lazy(() =>
//   import("./example-pages/ElementsSearchBars")
// );
// const ElementsTimelines = lazy(() =>
//   import("./example-pages/ElementsTimelines")
// );
// const ElementsUtilitiesHelpers = lazy(() =>
//   import("./example-pages/ElementsUtilitiesHelpers")
// );
// const BlocksChartsLarge = lazy(() =>
//   import("./example-pages/BlocksChartsLarge")
// );
// const BlocksChartsSmall = lazy(() =>
//   import("./example-pages/BlocksChartsSmall")
// );
// const BlocksComposed = lazy(() => import("./example-pages/BlocksComposed"));
// const BlocksContentText = lazy(() =>
//   import("./example-pages/BlocksContentText")
// );
// const BlocksGrids = lazy(() => import("./example-pages/BlocksGrids"));
// const BlocksIcons = lazy(() => import("./example-pages/BlocksIcons"));
// const BlocksImages = lazy(() => import("./example-pages/BlocksImages"));
// const BlocksListsLarge = lazy(() => import("./example-pages/BlocksListsLarge"));
// const BlocksListsSmall = lazy(() => import("./example-pages/BlocksListsSmall"));
// const BlocksNavigation = lazy(() => import("./example-pages/BlocksNavigation"));
// const BlocksProfilesSmall = lazy(() =>
//   import("./example-pages/BlocksProfilesSmall")
// );
// const BlocksProgressCircular = lazy(() =>
//   import("./example-pages/BlocksProgressCircular")
// );
// const BlocksProgressHorizontal = lazy(() =>
//   import("./example-pages/BlocksProgressHorizontal")
// );
// const BlocksSparklinesLarge = lazy(() =>
//   import("./example-pages/BlocksSparklinesLarge")
// );
// const BlocksSparklinesSmall = lazy(() =>
//   import("./example-pages/BlocksSparklinesSmall")
// );
// const BlocksStatistics = lazy(() => import("./example-pages/BlocksStatistics"));
// const MarketingCta = lazy(() => import("./example-pages/MarketingCta"));
// const MarketingFeatureSections = lazy(() =>
//   import("./example-pages/MarketingFeatureSections")
// );
// const MarketingFooters = lazy(() => import("./example-pages/MarketingFooters"));
// const MarketingHeaders = lazy(() => import("./example-pages/MarketingHeaders"));
// const MarketingHero = lazy(() => import("./example-pages/MarketingHero"));
// const MarketingIcons = lazy(() => import("./example-pages/MarketingIcons"));
// const MarketingPartners = lazy(() =>
//   import("./example-pages/MarketingPartners")
// );
// const MarketingPricingTables = lazy(() =>
//   import("./example-pages/MarketingPricingTables")
// );
// const MarketingTestimonials = lazy(() =>
//   import("./example-pages/MarketingTestimonials")
// );
// const WidgetsAccordions = lazy(() =>
//   import("./example-pages/WidgetsAccordions")
// );
// const WidgetsCalendars = lazy(() => import("./example-pages/WidgetsCalendars"));
// const WidgetsCarousels = lazy(() => import("./example-pages/WidgetsCarousels"));
// const WidgetsContextMenus = lazy(() =>
//   import("./example-pages/WidgetsContextMenus")
// );
// const WidgetsDragDrop = lazy(() => import("./example-pages/WidgetsDragDrop"));
// const WidgetsGuidedTours = lazy(() =>
//   import("./example-pages/WidgetsGuidedTours")
// );
// const WidgetsImageCrop = lazy(() => import("./example-pages/WidgetsImageCrop"));
// const WidgetsLoadingIndicators = lazy(() =>
//   import("./example-pages/WidgetsLoadingIndicators")
// );
// const WidgetsModals = lazy(() => import("./example-pages/WidgetsModals"));
// const WidgetsNotifications = lazy(() =>
//   import("./example-pages/WidgetsNotifications")
// );
// const WidgetsPopovers = lazy(() => import("./example-pages/WidgetsPopovers"));
// const WidgetsTabs = lazy(() => import("./example-pages/WidgetsTabs"));
// const WidgetsTooltips = lazy(() => import("./example-pages/WidgetsTooltips"));
// const WidgetsTreeView = lazy(() => import("./example-pages/WidgetsTreeView"));
// const ChartsApex = lazy(() => import("./example-pages/ChartsApex"));
// const ChartsGauges = lazy(() => import("./example-pages/ChartsGauges"));
// const Chartjs = lazy(() => import("./example-pages/Chartjs"));
// const ChartsSparklines = lazy(() => import("./example-pages/ChartsSparklines"));
// const Tables = lazy(() => import("./example-pages/Tables"));
// const Maps = lazy(() => import("./example-pages/Maps"));
// const FormsClipboard = lazy(() => import("./example-pages/FormsClipboard"));
// const FormsColorpicker = lazy(() => import("./example-pages/FormsColorpicker"));
// const FormsDatepicker = lazy(() => import("./example-pages/FormsDatepicker"));
// const FormsInputMask = lazy(() => import("./example-pages/FormsInputMask"));
// const FormsInputSelect = lazy(() => import("./example-pages/FormsInputSelect"));
// const FormsSlider = lazy(() => import("./example-pages/FormsSlider"));
// const FormsSteppers = lazy(() => import("./example-pages/FormsSteppers"));
// const FormsTextareaAutosize = lazy(() =>
//   import("./example-pages/FormsTextareaAutosize")
// );
// const FormsTimepicker = lazy(() => import("./example-pages/FormsTimepicker"));
// const FormsTypeahead = lazy(() => import("./example-pages/FormsTypeahead"));
// const FormsUpload = lazy(() => import("./example-pages/FormsUpload"));
// const FormsValidation = lazy(() => import("./example-pages/FormsValidation"));
// const FormsWysiwygEditor = lazy(() =>
//   import("./example-pages/FormsWysiwygEditor")
// );
// const PageCalendar = lazy(() => import("./example-pages/PageCalendar"));
// const PageChat = lazy(() => import("./example-pages/PageChat"));
// const PageProjects = lazy(() => import("./example-pages/PageProjects"));
// const PageFileManager = lazy(() => import("./example-pages/PageFileManager"));
// const PageAuthModals = lazy(() => import("./example-pages/PageAuthModals"));
// const PageLoginBasic = lazy(() => import("./example-pages/PageLoginBasic"));
// const PageLoginCover = lazy(() => import("./example-pages/PageLoginCover"));
// const PageLoginIllustration = lazy(() =>
//   import("./example-pages/PageLoginIllustration")
// );
// const PageLoginOverlay = lazy(() => import("./example-pages/PageLoginOverlay"));
// const PageRegisterBasic = lazy(() =>
//   import("./example-pages/PageRegisterBasic")
// );
// const PageRegisterCover = lazy(() =>
//   import("./example-pages/PageRegisterCover")
// );
// const PageRegisterIllustration = lazy(() =>
//   import("./example-pages/PageRegisterIllustration")
// );
// const PageRegisterOverlay = lazy(() =>
//   import("./example-pages/PageRegisterOverlay")
// );
// const PageRecoverBasic = lazy(() => import("./example-pages/PageRecoverBasic"));
// const PageRecoverCover = lazy(() => import("./example-pages/PageRecoverCover"));
// const PageRecoverIllustration = lazy(() =>
//   import("./example-pages/PageRecoverIllustration")
// );
// const PageRecoverOverlay = lazy(() =>
//   import("./example-pages/PageRecoverOverlay")
// );
// const PageProfile = lazy(() => import("./example-pages/PageProfile"));
// const PageInvoice = lazy(() => import("./example-pages/PageInvoice"));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const Routes = () => {
  const location = useLocation();

  const pageVariants = {
    initial: {
      opacity: 0,
      scale: 0.99,
    },
    in: {
      opacity: 1,
      scale: 1,
    },
    out: {
      opacity: 0,
      scale: 1.01,
    },
  };

  const pageTransition = {
    type: "tween",
    ease: "anticipate",
    duration: 0.4,
  };

  const SuspenseLoading = () => {
    return (
      <>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <ClimbingBoxLoader color={"#3c44b1"} loading={true} />
          </div>
        </div>
      </>
    );
  };
  return (
    <AnimatePresence>
      <Suspense fallback={<SuspenseLoading />}>
        <Switch>
          <Redirect exact from="/" to="/Login" />
          <Route path={["/Login"]}>
            <PresentationLayout>
              <Switch location={location} key={location.pathname}>
                <motion.div
                  initial="initial"
                  animate="in"
                  exit="out"
                  variants={pageVariants}
                  transition={pageTransition}
                >
                  <Route path="/Login" component={Login} />
                </motion.div>
              </Switch>
            </PresentationLayout>
          </Route>
          <Route path="/Register" component={Register} />
          <PrivateRoute
            path={[
              //"/DashboardMonitoring",

              //MY PATHS
              "/MedicalRecord",
              "/Profile",
              "/Doctors",
              "/Home",
              "/Patients",
              "/Profile-doctor",
              "/MedicalRecord_patient",
              "/CreateConsultation",
              "/CreateDoctor",
              "/AdminConsultations",
              "/Profile_agent",
              "/Chat",
              "/ChatTest",
              //END OF MY PATHS
            ]}
          >
            <LeftSidebar>
              <Switch location={location} key={location.pathname}>
                <motion.div
                  initial="initial"
                  animate="in"
                  exit="out"
                  variants={pageVariants}
                  transition={pageTransition}
                >
                  {/*MY ROUTES*/}

                  <PrivateRoute
                    path="/MedicalRecord"
                    component={MedicalRecord}
                  />
                  <PrivateRoute path="/Profile" component={Profile} />
                  <PrivateRoute path="/Patients" component={Patients} />
                  <PrivateRoute path="/Doctors" component={Doctors} />
                  <PrivateRoute path="/Home" component={Home} />
                  <PrivateRoute
                    path="/Profile-doctor"
                    component={ProfileDoctor}
                  />
                  <PrivateRoute
                    path="/MedicalRecord_patient/:id"
                    component={MedicalRecord_patient}
                  />
                  <PrivateRoute
                    path="/CreateConsultation"
                    component={AdminConsultations}
                  />
                  <PrivateRoute
                    path="/CreateDoctor"
                    component={AdminConsultations}
                  />
                  <PrivateRoute
                    path="/AdminConsultations"
                    component={AdminConsultations}
                  />
                  <PrivateRoute
                    path="/Profile_agent"
                    component={ProfileAgent}
                  />

                  {/* For The Chat */}
                  <Hoc>
                    <PrivateRoute
                      exact
                      path="/chat/:chatID/"
                      component={Chat}
                    />
                  </Hoc>
                </motion.div>
              </Switch>
            </LeftSidebar>
          </PrivateRoute>

          <Route
            path={
              [
                //"/PageCalendar",
                //MY IMPORTS
                // "/Consultation",
                // "/Consultation-doctor",
                // "/Consultation-patient",
              ]
            }
          >
            <CollapsedSidebar>
              <Switch location={location} key={location.pathname}>
                <motion.div
                  initial="initial"
                  animate="in"
                  exit="out"
                  variants={pageVariants}
                  transition={pageTransition}
                >
                  {/* <PrivateRoute path="/Consultation" component={Consultation} />
                  <PrivateRoute
                    path="/Consultation-doctor"
                    component={ConsultationDoctor}
                  />
                  <PrivateRoute
                    path="/Consultation-patient"
                    component={ConsultationPatient}
                  /> */}
                  {/* END MY ROUTES */}
                </motion.div>
              </Switch>
            </CollapsedSidebar>
          </Route>

          <Route>
            <MinimalLayout>
              <Switch location={location} key={location.pathname}>
                <motion.div
                  initial="initial"
                  animate="in"
                  exit="out"
                  variants={pageVariants}
                  transition={pageTransition}
                ></motion.div>
              </Switch>
            </MinimalLayout>
          </Route>
        </Switch>
      </Suspense>
    </AnimatePresence>
  );
};

export default Routes;
