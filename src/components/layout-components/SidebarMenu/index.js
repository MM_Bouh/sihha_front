import React from "react";
import {
  Activity,
  Box,
  ChevronRight,
  PlusCircle,
  UserPlus,
} from "react-feather";
import { FaBookMedical, FaHome, FaUserMd } from "react-icons/fa";
import PerfectScrollbar from "react-perfect-scrollbar";
import { connect } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { setSidebarToggleMobile } from "../../../reducers/ThemeOptions";
import SidebarUserbox from "../SidebarUserbox";

const SidebarMenu = (props) => {
  const history = useHistory();
  const { setSidebarToggleMobile, sidebarUserbox } = props;

  const toggleSidebarMobile = () => setSidebarToggleMobile(false);

  return (
    <>
      <PerfectScrollbar>
        {sidebarUserbox && <SidebarUserbox />}
        <div className="sidebar-navigation">
          <ul>
            {/*CHECK IF THE USER IS AUTHENTICATED  ===> CHECK THE ROLE FOR RENDERING THE NAV MENU ITEMS*/}
            {props.auth.isAuthenticated &&
            props.auth.user.role === "PATIENT" ? (
              <>
                <li>
                  <NavLink
                    activeClassName="active"
                    onClick={(e) => {
                      setSidebarToggleMobile(false);
                      history.push("/Home");
                    }}
                    className="nav-link-simple"
                    to="/Home"
                  >
                    <span className="sidebar-icon">
                      {/* <Users /> */}
                      {/* <MDBIcon icon="user-md" /> */}
                      <FaHome />
                    </span>
                    Home
                    <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                      <ChevronRight />
                    </span>
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    activeClassName="active"
                    onClick={(e) => {
                      setSidebarToggleMobile(false);
                      history.push("/Doctors");
                    }}
                    className="nav-link-simple"
                    to="/Doctors"
                  >
                    <span className="sidebar-icon">
                      {/* <Users /> */}
                      {/* <MDBIcon icon="user-md" /> */}
                      <FaUserMd />
                    </span>
                    Doctors
                    <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                      <ChevronRight />
                    </span>
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    activeClassName="active"
                    onClick={(e) => {
                      setSidebarToggleMobile(false);
                      history.push("/MedicalRecord");
                    }}
                    className="nav-link-simple"
                    to="/MedicalRecord"
                  >
                    <span className="sidebar-icon">
                      {/* <Box /> */}
                      <FaBookMedical />
                    </span>
                    Medical Record
                    <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                      <ChevronRight />
                    </span>
                  </NavLink>
                </li>
              </>
            ) : (
              <>
                {props.auth.isAuthenticated &&
                props.auth.user.role === "DOCTOR" ? (
                  <>
                    <li>
                      <NavLink
                        activeClassName="active"
                        onClick={toggleSidebarMobile}
                        className="nav-link-simple"
                        to="/Patients"
                      >
                        <span className="sidebar-icon">
                          <Box />
                        </span>
                        Patients
                        <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                          <ChevronRight />
                        </span>
                      </NavLink>
                    </li>
                    {/* <li>
                      <NavLink
                        activeClassName="active"
                        onClick={toggleSidebarMobile}
                        className="nav-link-simple"
                        to="/Consultation-doctor"
                      >
                        <span className="sidebar-icon">
                          <Activity />
                        </span>
                        Consultation
                        <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                          <ChevronRight />
                        </span>
                      </NavLink>
                    </li> */}
                  </>
                ) : props.auth.isAuthenticated &&
                  props.auth.user.role === "ADMIN_AGENT" ? (
                  <>
                    <li>
                      <NavLink
                        activeClassName="active"
                        onClick={toggleSidebarMobile}
                        className="nav-link-simple"
                        to="/CreateConsultation"
                      >
                        <span className="sidebar-icon">
                          <PlusCircle />
                        </span>
                        Create Consultation
                        <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                          <ChevronRight />
                        </span>
                      </NavLink>
                    </li>
                    <li>
                      <NavLink
                        activeClassName="active"
                        onClick={toggleSidebarMobile}
                        className="nav-link-simple"
                        to="/CreateDoctor"
                      >
                        <span className="sidebar-icon">
                          <UserPlus />
                        </span>
                        Create Doctor
                        <span className="sidebar-icon-indicator sidebar-icon-indicator-right">
                          <ChevronRight />
                        </span>
                      </NavLink>
                    </li>
                  </>
                ) : (
                  ""
                )}
              </>
            )}
          </ul>
        </div>
      </PerfectScrollbar>
    </>
  );
};

const mapStateToProps = (state) => ({
  sidebarUserbox: state.ThemeOptions.sidebarUserbox,
  sidebarToggleMobile: state.ThemeOptions.sidebarToggleMobile,
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({
  setSidebarToggleMobile: (enable) => dispatch(setSidebarToggleMobile(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarMenu);
