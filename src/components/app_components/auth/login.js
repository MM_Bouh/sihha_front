import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { connect } from "react-redux";
import {
  Link,
  Redirect,
  useHistory,
  useLocation,
  withRouter,
} from "react-router-dom";
import { ClimbingBoxLoader } from "react-spinners";
import { toast } from "react-toastify";
import { Button, Col, FormGroup, Input } from "reactstrap";
import { login } from "../../../actions/auth";

const successNotify = (res, message) => {
  if (res === "success") {
    toast.success(message, { containerId: "B" });
  } else if (res === "fail") {
    toast.error(message, { containerId: "B" });
  }
};
function Login(props) {
  const history = useHistory();
  const details = props.auth;
  const { isLoading, isAuthenticated, error } = details;

  const onFinish = (e) => {
    e.preventDefault();
    props.login(tel, password);
  };
  const [tel, settel] = useState([]);
  const handleChange = (e) => {
    settel(e.target.value);
  };
  const [password, setPassword] = useState([]);
  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };
  const [showPassword, setShowPassword] = useState(false);

  const UserRouteComponent = () => {
    const location = useLocation();
    let userRoute = "/";
    if (props.auth.user.role === "PATIENT") {
      userRoute = "/Home";
    } else if (props.auth.user.role === "DOCTOR") {
      userRoute = "/Patients";
    } else if (props.auth.user.role === "ADMIN_AGENT") {
      userRoute = "/AdminConsultations";
    }
    console.log("location", location);

    const { from } = location.state || { from: { pathname: userRoute } };

    console.log(from);

    return <Redirect exact to={from} />;
  };
  const SuspenseLoading = () => {
    return (
      <>
        <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
          <div className="d-flex align-items-center flex-column px-4">
            <ClimbingBoxLoader color={"#3c44b1"} loading={true} />
          </div>
        </div>
      </>
    );
  };
  return (
    <>
      {/*CHECK IF THE USER IS AUTHENTICATED => CHECK THE ROLE*/}
      {isAuthenticated ? (
        <UserRouteComponent />
      ) : (
        // successNotify("success", "Success, welcome back!")
        isLoading && <SuspenseLoading />
      )}
      {/* Here you nedd to check if the user's credentials are true*/}
      {/* {error &&
        successNotify(
          "fail",
          "No active account found with the given credentials"
        )} */}

      <div className="app-wrapper bg-white min-vh-100">
        <div className="app-main min-vh-100">
          <div className="app-content p-0">
            <div className="app-content--inner d-flex align-items-center">
              <div className="flex-grow-1 w-100 d-flex align-items-center">
                <div className="bg-composed-wrapper--content py-5">
                  <Col md="10" lg="8" xl="4" className="mx-auto">
                    <div className="text-center">
                      <h1 className="display-4 mb-5 font-weight-bold">Login</h1>
                    </div>

                    <div>
                      <form onSubmit={onFinish} action="#">
                        <div className="form-group mb-3">
                          <Input
                            placeholder="Mobile Number"
                            type="telephone"
                            name="tel"
                            value={tel}
                            onChange={handleChange}
                          />
                        </div>
                        <FormGroup>
                          <Input
                            autoFocus
                            placeholder="Password"
                            type="password"
                            name="password"
                            value={password}
                            onChange={handleChangePassword}
                            className="input"
                          />

                          {/* <Input
                            autoFocus
                            placeholder="Password"
                            type={showPassword ? "text" : "password"}
                            name="password"
                            value={password}
                            onChange={handleChangePassword}
                            className="input"
                          />
                          <span className="icon is-small is-right">
                            <i
                              onClick={() =>
                                setShowPassword((showPassword) => !showPassword)
                              }
                            >
                              {showPassword ? (
                                <FontAwesomeIcon
                                  icon={["fas", "eye"]}
                                  className="text-warning mr-1"
                                />
                              ) : (
                                <FontAwesomeIcon icon={["fas", "eye-slash"]} />
                              )}
                            </i>
                          </span> */}
                        </FormGroup>
                        <div className="d-flex justify-content-between"></div>
                        <div className="text-center py-4">
                          <Button
                            type="submit"
                            color="second"
                            className="font-weight-bold w-50 my-2"
                          >
                            Sign in
                          </Button>
                        </div>
                      </form>
                      <div className="text-center text-black-50 mt-3">
                        Don't have an account?{" "}
                        {/* <a href="#" className="text-first"> */}
                        <Link
                          to="/Register"
                          className="text-uppercase font-weight-bold font-size-sm my-3"
                        >
                          Sign up
                        </Link>
                        {/* </a> */}
                      </div>
                    </div>
                  </Col>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, { login })(withRouter(Login));
