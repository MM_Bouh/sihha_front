import axios from "axios";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";
import { Badge, Card, CardBody, Table, Input } from "reactstrap";
//Components import
import ModalConsultation from './create_consultation';
import ModalDoctor from './create_doctor';





function LivePreviewExample(props) {
    const location = useLocation();
   
    useEffect(() => {

        axios
        // .get('https://medicare-back.herokuapp.com/chat-api/admin_agent_conversations/'+props.user.id)
        .get('http://localhost:8000/chat-api/admin_agent_conversations/'+props.user.id)
        .then((res) => {
          res.data = res.data.reverse()
          setConsultations(res.data)
          console.log(res.data)
          
        })

    }, []);

  //STATES
  const [consultations, setConsultations] = useState([]);
  const [modal4,setModal4] = useState(true)
  const toggle4 = () => setModal4(!modal4);
  const [status, setStatus] = useState("")

  const handleStatus = (e) => {
    setStatus(e.target.value)
  };
  return (
    <>
      { location.pathname=='/CreateConsultation' && <ModalConsultation user={props.user.id}/>}
      { location.pathname=='/CreateDoctor' && <ModalDoctor user={props.user.id}/>}
      <Card className="card-box mb-5">
        <div className="card-header pr-2">
          <div className="card-header--title">Consultations</div>
        </div>
        <CardBody>
          <div className="table-responsive-md">
            <Table hover borderless className="text-nowrap mb-0">
              <thead>
                <tr>
                  <th className="text-center">Consutation Id</th>
                  <th className="text-center">Consultation Status</th>
                  <th className="text-center">Doctor</th>
                  <th className="text-center">Patient</th>
                </tr>
              </thead>
              <tbody>
                {/*FETCH THE consultations*/}
                {consultations && consultations[0] && consultations.map((consultation, index) => <tr key={index}>
                  <td className="text-center">
                  <span className="font-weight-bold text-black">{consultation.id}</span>
                  </td>

                  <td className="text-center">
                  <Badge color={consultation.state === 'PENDING' ?'warning':(consultation.state === 'ACTIVE'?'success':'first')} className="h-auto py-0 px-3">
                  {consultation.state}
                  </Badge>
                  
                  </td>
                  
                  {/* Here you need to give the admin the ability 
                  to change the status of a conversation intead of just showing them */}
                  {/* <td className="text-center">
                  <Input
                  id="exampleCustomSelect"
                  name="customSelect"
                  type="select"
                  onChange={handleStatus}
                  value={status}
                  >
                  
                  <option >PENDING</option>
                  <option>ACTIVE</option>
                  <option>COMPLETE</option>
                  </Input>
                  
                  </td> */}
                  <td className="text-center">
                  <span className="font-weight-bold text-black">Dr. {consultation.doctor}</span>
                  </td>
                  <td className="text-center">
                  <span className="font-weight-bold text-black"> {consultation.patient}</span>
                  </td>
                </tr>)}
                
              </tbody>
            </Table>
          </div>
        </CardBody>
      </Card>
    </>
  );
}
const mapStateToProps = state => {
  return {
    user: state.auth.user,
    
  };
};
export default connect(mapStateToProps)(LivePreviewExample);








